lazy val pxsVersion = getVersion

val akkaHttpCorsV = "0.3.0"
val akkaHttpV     = "10.1.3"
val cfgV          = "0.3.0"
val logbackV      = "1.2.3"
val mongoV        = "2.1.0"
val pprintV       = "0.5.2"
val scalaLoggingV = "3.7.2"
val scalatestV    = "3.0.3"
val scalaV        = "2.12.4"
val swaggerAkkaV  = "0.14.0"

name := "pxs"

lazy val pxs = project
  .settings(
    name := "pxs",
    version := pxsVersion,
    scalaVersion := scalaV,
    libraryDependencies ++= Seq(
      "com.typesafe.akka"    %% "akka-http"            % akkaHttpV,
      "com.typesafe.akka"    %% "akka-http-spray-json" % akkaHttpV,
      "com.typesafe.akka"    %% "akka-http-testkit"    % akkaHttpV,
      "ch.megard"            %% "akka-http-cors"       % akkaHttpCorsV,
      "com.github.carueda"   %% "cfg"                  % cfgV % "provided",
      "org.mongodb.scala"    %% "mongo-scala-driver"   % mongoV,
      "org.scalatest"        %% "scalatest"            % scalatestV % "test"

      ,"com.lihaoyi"         %% "pprint"               % pprintV

      ,"com.github.swagger-akka-http" %% "swagger-akka-http" % swaggerAkkaV

      ,"com.typesafe.scala-logging"   %% "scala-logging"   % scalaLoggingV
      ,"ch.qos.logback"                % "logback-classic" % logbackV
    ),
    addCompilerPlugin(
      ("org.scalameta" % "paradise" % "3.0.0-M10").cross(CrossVersion.full)
    ),

    parallelExecution in Test := false,

    mainClass in assembly := Some("pxs.Main"),
    assemblyJarName in assembly := s"pxs-$pxsVersion.jar",

    scalacOptions ++= Seq("-deprecation", "-feature", "-encoding", "utf8"
      , "-Ywarn-dead-code"
      , "-unchecked"
      , "-Xlint"
      , "-Ywarn-unused-import"
    )
  )

def getVersion: String = {
  val version = {
    val refFile = file("pxs/src/main/resources/reference.conf")
    val versionRe = """pxs\.version\s*=\s*(.+)""".r
    IO.read(refFile).trim match {
      case versionRe(v) ⇒ v
      case _ ⇒ sys.error(s"could not parse pxs.version from $refFile")
    }
  }
  println(s"pxs.version = $version")
  version
}
