2018-08-07

- remove `TaskDefAddResponse`; just use `TaskDefAdd`
- remove `PlanGetResponse`; just use `Plan`
- report 404 for getTaskDef

2018-08-06

- set cors in master service
- update akk-http and related libs

2018-07-19

- change dir layout to have `pxs` as a sbt submodule.
  This also allows to continue testing building with mill.

2018-07-18

- a first and pretty successful test of building pxs with
  [mill](https://github.com/lihaoyi/mill). See build.sc
- allow to continue with defaults if conf/pxs.conf cannot be read

2018-04-05

- remove ams model
- rename project to pxs

2018-03-19

- api: add crud for arguments
- general renaming: "ams" instead of "org"
- api: add crud for parameters
- api: add plan only registers the entry; tasks to be added separately
  FIXME: adjust tests for actual async execution
- general renaming: "Task" instead of "SchedTask"
- complete (basic) CRUD for Task
- check asset class in submission of  task
- adjust protocol to reflect that TaskDef belongs to an Executor

2018-03-16

- model change:
    - Asset now has an 'assetClass'
    - TaskDef is associated with a collection of 'asset classes'
      (and not with specific asset instances)
    - NOTE: 'assetClass' is simply represented with a string, that is,
      it's not a model per se in PXS, at least for now.
- model: introduce Argument to set parameter values in a Task
- model: use Seq for Plan's Task's, not Map
- model: use Seq for TaskDef's parameters, not Map

2018-03-15

- api: use plural for consistency: assets, plans, taskdefs
- add api documentation while reviewing model
- upgrade swagger-ui to latest at this moment, 3.12.1
  (reminder: swagger-ui is not kept under version control here)
- resuming work

2017-12

- preliminaries with model, mongodb, ci
