//
// A first and pretty successful test of building pxs with mill.
//
//     mill pxs.run run-server
//     mill pxs.test
//     mill pxs.assembly
//     java -jar out/pxs/assembly/dest/out.jar run-server
//

import mill._
import mill.scalalib._
import mill.scalalib.publish._
import ammonite.ops._

lazy val pxsVersion = getVersion

val akkaHttpCorsV = "0.3.0"
val akkaHttpV     = "10.1.3"
val cfgV          = "0.3.0"
val logbackV      = "1.2.3"
val mongoV        = "2.1.0"
val pprintV       = "0.5.2"
val scalaLoggingV = "3.7.2"
val scalatestV    = "3.0.3"
val scalaV        = "2.12.4"
val swaggerAkkaV  = "0.14.0"

object pxs extends SbtModule with PublishModule {
  def scalaVersion = scalaV

  def publishVersion = pxsVersion

  override def mainClass = Some("pxs.Main")

  def pomSettings = PomSettings(
    description = artifactName(),
    organization = "org.mbari",
    url = "https://github.com/mbari-pxs/pxs",
    licenses = Seq(License.MIT),
    versionControl = VersionControl.github("carueda", "pxs"),
    developers = Seq(
      Developer("carueda", "Carlos Rueda","https://github.com/carueda")
    )
  )

  override def ivyDeps = Agg(
    ivy"com.typesafe.akka::akka-http:$akkaHttpV",
    ivy"com.typesafe.akka::akka-http-spray-json:$akkaHttpV",
    ivy"com.typesafe.akka::akka-http-testkit:$akkaHttpV",
    ivy"ch.megard::akka-http-cors:$akkaHttpCorsV",
    ivy"com.github.carueda::cfg:$cfgV",
    ivy"org.mongodb.scala::mongo-scala-driver:$mongoV",
    ivy"com.lihaoyi::pprint:$pprintV",
    ivy"com.github.swagger-akka-http::swagger-akka-http:$swaggerAkkaV",
    ivy"com.typesafe.scala-logging::scala-logging:$scalaLoggingV",
    ivy"ch.qos.logback:logback-classic:$logbackV",
  )

  private val scalametaParadise = ivy"org.scalameta:paradise_2.12.4:3.0.0-M11"

  def compileIvyDeps = Agg(scalametaParadise)

  def scalacPluginIvyDeps = Agg(scalametaParadise)

  object test extends Tests {
    override def ivyDeps = Agg(ivy"org.scalatest::scalatest:$scalatestV")
    override def testFrameworks = Seq("org.scalatest.tools.Framework")
  }

  override def scalacOptions = Seq(
    "-deprecation", "-feature", "-encoding", "utf8",
    "-Ywarn-dead-code",
    "-unchecked",
    "-Xlint",
    "-Ywarn-unused-import",
  )
}

def getVersion: String = {
  val version = {
    val versionRe = """pxs\.version\s*=\s*(.+)""".r
    val refFile = pwd/'pxs/'src/'main/'resources/"reference.conf"
    read(refFile).trim match {
      case versionRe(v) ⇒ v
      case _ ⇒ sys.error(s"could not parse pxs.version from $refFile")
    }
  }
  println(s"pxs.version = $version")
  version
}
