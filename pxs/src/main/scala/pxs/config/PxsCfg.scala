package pxs.config

import carueda.cfg._

@Cfg
case class PxsCfg(
                   externalUrl: String = "http://localhost:5050",
                   httpInterface: String = "0.0.0.0",
                   httpPort: Int = 5050,
                   mongo: Option[MongoCfg]
                 ) {

  object pxs {
    val version: String = $ // from reference.conf
  }
}

@Cfg
case class MongoCfg(
                     host: String = "localhost",
                     database: String = "pxs",
                     plans: String = "plans",
                     tokens: String = "tokens",
                     username: Option[String] = None,
                     password: Option[String] = None
                   )
