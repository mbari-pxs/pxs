package pxs

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}
import fansi.Color.Yellow

package object config {
  val configFile: File = new File("./conf", "pxs.conf")
  lazy val tsConfig: Config = {
    val configPath = configFile.getCanonicalPath
    if (configFile.canRead) {
      println(Yellow(s"Configuration file: $configPath"))
      ConfigFactory.parseFile(configFile)
        .withFallback(ConfigFactory.load())
    }
    else {
      println(Yellow(s"Using cfg defaults (cannot read $configPath)"))
      ConfigFactory.load()
    }
  }.resolve()
  lazy val cfg: PxsCfg = PxsCfg(tsConfig)

}
