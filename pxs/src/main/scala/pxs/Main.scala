package pxs

import pxs.server.Server

object Main {
  def main(args: Array[String]): Unit = {
    if (args.contains("run-server")) {
      new Server().run(keyToStop = !args.contains("-d"))
    }
    else {
      System.err.println(
        s"""
           |Usage:
           |   pxs run-server [-d]
        """.stripMargin)
    }
  }
}
