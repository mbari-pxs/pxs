package pxs.model

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import io.swagger.annotations.{ApiModel, ApiModelProperty}
import spray.json.{DefaultJsonProtocol, JsValue}

import scala.annotation.meta.field

case class ExecutorAdd(
  @ApiModelProperty(value = "Executor ID.")
  executorId:       String,
  @ApiModelProperty(value = "HTTP endpoint of the executor.")
  httpEndpoint:     String,
  @ApiModelProperty(value = "A description of this executor (optional).")
  description:      Option[String]   = None
)

case class ExecutorAddResponse(
  executorId:       String,
  httpEndpoint:     String,
  description:      Option[String]   = None
)

case class ExecutorGetResponse(
  executorId:       String,
  httpEndpoint:     String,
  description:      Option[String]   = None
)

case class ExecutorUpdate(
  httpEndpoint:     Option[String]   = None,
  description:      Option[String]   = None
)


/////////////////////////////////////////////////
// Asset

case class AssetUpdate(
  @ApiModelProperty(value = "ID of the asset's class.")
  assetClass:    Option[String] = None,

  @ApiModelProperty(value = "A description of this asset")
  description:   Option[String]  = None,
)

case class AssetUpdateResponse(
  assetId:      String
)

/////////////////////////////////////////////////
// TaskDef

case class TaskDefAdd(
  @ApiModelProperty(value = "Task definition ID.")
  taskDefId:        String,

  @ApiModelProperty(value = "Associated asset classes.")
  assetClasses:     Seq[String]              = Seq.empty,

  @ApiModelProperty(value = "A description of this task definition (optional).")
  description:      Option[String]           = None,
)

case class TaskDefUpdate(
  @ApiModelProperty(value = "Associated asset classes.")
  assetClasses:   Option[Seq[String]]      = None,

  @ApiModelProperty(value = "A description of this task definition (optional).")
  description:    Option[String]           = None,
)

case class TaskDefUpdateResponse(
  taskDefId:      String
)

/////////////////////////////////////////////////
// Parameter

case class ParameterAddResponse(
  name:        String,
)

case class ParameterUpdate(
  @ApiModelProperty(value = "The type of the parameter.")
  `type`:           Option[String] = None,

  @ApiModelProperty(value = "Is the parameter required?")
  required:         Option[Boolean] = None,

  @ApiModelProperty(value = "Default parameter value (optional).")
  defaultValue:     Option[String] = None,

  @ApiModelProperty(value = "Description of the parameter (optional).")
  description:      Option[String] = None
)

case class ParameterUpdateResponse(
  name:      String
)

case class ParameterDeleteResponse(
  name:      String
)

/////////////////////////////////////////////////
// Plan

@ApiModel(description="Defines a requested task in a plan.")
case class TaskAdd(
  @ApiModelProperty(value = "ID of associated executor..")
  executorId:       String,

  @ApiModelProperty(value = "ID of associated task definition.")
  taskDefId:        String,

  @ApiModelProperty(value = "Values for relevant parameters in the task definition.")
  arguments:        Option[Seq[Argument]] = None,

  @ApiModelProperty(value = "ID of associated asset.")
  assetId:          String,

  @ApiModelProperty(value = "Name (optional).")
  name:             Option[String] = None,

  @ApiModelProperty(value = "Description (optional).")
  description:      Option[String] = None,

  @(ApiModelProperty @field)(dataType = "dateTime", value = "Start date-time (optional).")
  start:            Option[String] = None,

  @(ApiModelProperty @field)(dataType = "dateTime", value = "End date-time (optional).")
  end:              Option[String] = None,

  @(ApiModelProperty @field)(dataType = "object", value = "Associated geometry (optional).")
  geometry:         Option[JsValue] = None,
)

@ApiModel(description="Defines attributes that can be updated in a task.")
case class TaskUpdate(
  @ApiModelProperty(value = "ID of associated task definition.")
  taskDefId:        Option[String] = None,

  @ApiModelProperty(value = "Values for relevant parameters in the task definition.")
  arguments:        Option[Seq[Argument]] = None,

  @ApiModelProperty(value = "ID of associated asset.")
  assetId:          Option[String] = None,

  @ApiModelProperty(value = "Name (optional).")
  name:             Option[String] = None,

  @ApiModelProperty(value = "Description (optional).")
  description:      Option[String] = None,

  @(ApiModelProperty @field)(dataType = "dateTime", value = "Start date-time (optional).")
  start:            Option[String] = None,

  @(ApiModelProperty @field)(dataType = "dateTime", value = "End date-time (optional).")
  end:              Option[String] = None,

  @(ApiModelProperty @field)(dataType = "object", value = "Associated geometry (optional).")
  geometry:         Option[JsValue] = None,
)

@ApiModel(description="Defines request to create a new plan.")
case class PlanAdd(
  @ApiModelProperty(value = "Name for this plan.")
  name:           String,

  @ApiModelProperty(value = "Description of the plan (optional).")
  description:    Option[String] = None,
)

case class PlanAddResponse(
  @ApiModelProperty(value = "Assigned plan ID.")
  planId:  String,

  @ApiModelProperty(value = "Name given to the added plan.")
  name:    String
)

case class PlanUpdate(
  name:   Option[String]
)

case class PlanUpdateResponse(
  planId:  String
)

case class TaskAddResponse(
  @ApiModelProperty(value = "Associated plan ID.")
  planId:  String,

  @ApiModelProperty(value = "Assigned task ID.")
  taskId:  String
)

case class TaskDeleteResponse(
  @ApiModelProperty(value = "Associated plan ID.")
  planId:  String,

  @ApiModelProperty(value = "Assigned task ID.")
  taskId:  String
)

case class TaskUpdateResponse(
  @ApiModelProperty(value = "Associated plan ID.")
  planId:  String,

  @ApiModelProperty(value = "Assigned task ID.")
  taskId:  String
)

case class ArgumentUpdate(
  @ApiModelProperty(value = "Value for the parameter.")
  paramValue:       String
)


/////////////////////////////////////////////////
// generic error

case class GnError(code: Int,
                   msg:  String,
                   id:   Option[String] = None,
                   tkid: Option[String] = None
                  )

object GnErrorF {
  def defined(modelName: String, id: String): GnError =
    GnError(409, s"$modelName already defined", id = Some(id))

  def undefined(modelName: String, id: String): GnError =
    GnError(404, s"$modelName undefined", id = Some(id))

  def tokenDefined(tkid: String): GnError =
    GnError(409, "token already defined", tkid = Some(tkid))

  def tokenUndefined(tkid: String): GnError =
    GnError(404, "token undefined", tkid = Some(tkid))

}

trait PxsJsonImplicits extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val _Asset                 = jsonFormat3(Asset)
  implicit val _Parameter             = jsonFormat5(Parameter)
  implicit val _TaskDef               = jsonFormat4(TaskDef)
  implicit val _Argument              = jsonFormat2(Argument)
  implicit val _Task                  = jsonFormat9(Task)
  implicit val _Plan                  = jsonFormat4(Plan)
  implicit val _Executor              = jsonFormat5(Executor)

  implicit val _AssetUpdate           = jsonFormat2(AssetUpdate)
  implicit val _AssetUpdateResponse   = jsonFormat1(AssetUpdateResponse)

  implicit val _TaskDefAdd            = jsonFormat3(TaskDefAdd)
  implicit val _TaskDefUpdate         = jsonFormat2(TaskDefUpdate)
  implicit val _TaskDefUpdateResponse = jsonFormat1(TaskDefUpdateResponse)

  implicit val _ParameterAddResponse  = jsonFormat1(ParameterAddResponse)
  implicit val _ParameterUpdate       = jsonFormat4(ParameterUpdate)
  implicit val _ParameterUpdateResponse = jsonFormat1(ParameterUpdateResponse)
  implicit val _ParameterDeleteResponse = jsonFormat1(ParameterDeleteResponse)

  implicit val _TaskAdd               = jsonFormat9(TaskAdd)
  implicit val _TaskAddResponse       = jsonFormat2(TaskAddResponse)
  implicit val _TaskDeleteResponse    = jsonFormat2(TaskDeleteResponse)
  implicit val _TaskUpdate            = jsonFormat8(TaskUpdate)
  implicit val _TaskUpdateResponse    = jsonFormat2(TaskUpdateResponse)

  implicit val _ArgumentUpdate        = jsonFormat1(ArgumentUpdate)

  implicit val _PlanAdd               = jsonFormat2(PlanAdd)
  implicit val _PlanUpdate            = jsonFormat1(PlanUpdate)
  implicit val _PlanAddResponse       = jsonFormat2(PlanAddResponse)
  implicit val _PlanUpdateResponse    = jsonFormat1(PlanUpdateResponse)

  implicit val _ExecutorAdd           = jsonFormat3(ExecutorAdd)
  implicit val _ExecutorAddResponse   = jsonFormat3(ExecutorAddResponse)
  implicit val _ExecutorGetResponse   = jsonFormat3(ExecutorGetResponse)
  implicit val _ExecutorUpdate        = jsonFormat2(ExecutorUpdate)

  implicit val _GnError               = jsonFormat4(GnError)
}
