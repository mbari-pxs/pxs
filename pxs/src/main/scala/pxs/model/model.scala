package pxs.model

import io.swagger.annotations.{ApiModel, ApiModelProperty}
import spray.json.JsValue

import scala.annotation.meta.field

@ApiModel(description="Defines an asset in an Executor. " +
  "Tasks in a plan are associated with specific assets.")
case class Asset(
  @ApiModelProperty(value = "Asset ID.")
  assetId:       String,

  @ApiModelProperty(value = "ID of the asset's class.")
  assetClass:       String,

  @ApiModelProperty(value = "A description of this asset (optional).")
  description:   Option[String]  = None,
)

@ApiModel(description="Defines an executor. " +
  "An executor is able to execute tasks according to given task definitions, " +
  "and has an associated HTTP endpoint that PXS uses for purposes " +
  "of requesting the execution of concrete tasks.")
case class Executor(
  @ApiModelProperty(value = "Executor ID.")
  executorId:       String,

  @ApiModelProperty(value = "HTTP endpoint of the executor.")
  httpEndpoint:     String,

  @ApiModelProperty(value = "A description of this executor (optional).")
  description:      Option[String]    = None,

  @ApiModelProperty(value = "The assets that can be planned")
  assets:           Seq[Asset]       = Seq.empty,

  @ApiModelProperty(value = "Definition of the tasks this executor can execute.")
  taskDefs:         Seq[TaskDef]      = Seq.empty
)

case class TaskDef(
  @ApiModelProperty(value = "Task definition ID.")
  taskDefId:        String,

  @ApiModelProperty(value = "Associated asset classes.")
  assetClasses:     Seq[String]              = Seq.empty,

  @ApiModelProperty(value = "A description of this task definition (optional).")
  description:      Option[String]           = None,

  @ApiModelProperty(value = "The parameters of this task definition.")
  parameters:       Seq[Parameter]           = Seq.empty
)

@ApiModel(description="Defines a parameter in a task definition.")
case class Parameter(
  @ApiModelProperty(value = "The name of the parameter.")
  name:             String,

  @ApiModelProperty(value = "The type of the parameter.")
  `type`:           String,

  @ApiModelProperty(value = "Is the parameter required?")
  required:         Boolean,

  @ApiModelProperty(value = "Default parameter value (optional).")
  defaultValue:     Option[String] = None,

  @ApiModelProperty(value = "Description of the parameter (optional).")
  description:      Option[String] = None
)


@ApiModel(description="Defines a concrete plan with associated tasks.")
case class Plan(
  @ApiModelProperty(value = "Plan ID.")
  planId:           String,

  @ApiModelProperty(value = "Name of the plan.")
  name:             String,

  @ApiModelProperty(value = "Description of the plan.")
  description:      Option[String] = None,

  @ApiModelProperty(value = "The tasks associated to this plan.")
  tasks:            Seq[Task] = Seq.empty
)

@ApiModel(description="Defines a task in a plan.")
case class Task(
  @ApiModelProperty(value = "ID of this task.")
  taskId:      String,

  @ApiModelProperty(value = "ID of associated task definition.")
  taskDefId:        String,

  @ApiModelProperty(value = "Explicit values for relevant parameters in the task definition.")
  arguments:        Seq[Argument] = Seq.empty,

  @ApiModelProperty(value = "ID of associated asset.")
  assetId:          String,

  @ApiModelProperty(value = "Name (optional).")
  name:             Option[String] = None,

  @ApiModelProperty(value = "Description (optional).")
  description:      Option[String] = None,

  @(ApiModelProperty @field)(dataType = "dateTime", value = "Start date-time (optional).")
  start:            Option[String] = None,

  @(ApiModelProperty @field)(dataType = "dateTime", value = "End date-time (optional).")
  end:              Option[String] = None,

  @(ApiModelProperty @field)(dataType = "object", value = "Associated geometry (optional).")
  geometry:         Option[JsValue] = None,
)

@ApiModel(description="Defines an argument (parameter value) in a requested task.")
case class Argument(
  @ApiModelProperty(value = "Name of the parameter.")
  paramName:        String,

  @ApiModelProperty(value = "Value for the parameter.")
  paramValue:       String
)

