package pxs.server

import javax.ws.rs.Path

import akka.http.scaladsl.server.Route
import io.swagger.annotations._
import pxs.model._
import pxs.server.serviceimpl.ExecutorServiceImpl


@Path("/executors")
trait ExecutorService extends ExecutorServiceImpl with BaseService {
  def executorRoute: Route = {
    executorGetDetailed ~
    executorGet ~
    executorList ~
    executorAdd ~
    executorUpdate ~
    executorDelete
  }

  @ApiOperation(value = "Get all executors", nickname = "getExecutors",
    tags = Array("executor"),
    httpMethod = "GET",
    response = classOf[Array[ExecutorGetResponse]]
  )
  def executorList: Route = {
    path("api" / "executors") {
      get {
        complete {
          listExecutors
        }
      }
    }
  }

  @ApiOperation(value = "Add an executor", nickname = "addExecutor",
    tags = Array("executor"),
    httpMethod = "POST", code = 201,
    response = classOf[ExecutorAddResponse])
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "body", value = "executor definition", required = true,
      dataTypeClass = classOf[ExecutorAdd], paramType = "body")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 409, message = "executor already registered")
  ))
  def executorAdd: Route = {
    path("api" / "executors") {
      (post & entity(as[ExecutorAdd])) { a ⇒
        complete {
          addExecutor(a)
        }
      }
    }
  }

  @Path("/{executorId}/detailed")
  @ApiOperation(value = "Get a particular executor in detail", nickname = "getExecutorDetailed",
    tags = Array("executor"),
    httpMethod = "GET",
    response = classOf[Executor]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "executor id", required = true,
      dataType = "string", paramType = "path")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined executor")
  ))
  def executorGetDetailed: Route = {
    path("api" / "executors" / Segment / "detailed") { executorId ⇒
      get {
        complete {
          getExecutorDetailed(executorId)
        }
      }
    }
  }

  @Path("/{executorId}")
  @ApiOperation(value = "Get a particular executor", nickname = "getExecutor",
    tags = Array("executor"),
    httpMethod = "GET",
    response = classOf[ExecutorGetResponse]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "executor id", required = true,
      dataType = "string", paramType = "path")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined executor")
  ))
  def executorGet: Route = {
    path("api" / "executors" / Segment) { executorId ⇒
      get {
        complete {
          getExecutor(executorId)
        }
      }
    }
  }

  @Path("/{executorId}")
  @ApiOperation(value = "Update an executor", nickname = "updateExecutor",
    tags = Array("executor"),
    httpMethod = "PUT",
    response = classOf[ExecutorGetResponse]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "body", value = "Properties to update. All elements are optional.",
      required = true,
      dataTypeClass = classOf[ExecutorUpdate], paramType = "body")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined executor")
  ))
  def executorUpdate: Route = {
    path("api" / "executors" / Segment) { executorId ⇒
      (put & entity(as[ExecutorUpdate])) { u ⇒
        complete {
          updateExecutor(executorId, u)
        }
      }
    }
  }

  @Path("/{executorId}")
  @ApiOperation(value = "Delete an executor", nickname = "deleteExecutor",
    tags = Array("executor"),
    httpMethod = "DELETE",
    response = classOf[ExecutorGetResponse]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "executor id", required = true,
      dataType = "string", paramType = "path")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined executor")
  ))
  def executorDelete: Route = {
    path("api" / "executors" / Segment) { executorId ⇒
      delete {
        complete {
          deleteExecutor(executorId)
        }
      }
    }
  }
}
