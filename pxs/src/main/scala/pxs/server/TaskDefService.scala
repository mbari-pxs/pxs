package pxs.server

import akka.http.scaladsl.server.Route
import io.swagger.annotations._
import javax.ws.rs.Path
import pxs.model._
import pxs.server.serviceimpl.ExecutorServiceImpl

@Path("/executors")
trait TaskDefService extends ExecutorServiceImpl with BaseService {
  def taskDefRoute: Route = {
    paramGet ~
    paramUpdate ~
    paramDelete ~
    paramAdd ~
    paramList ~
    taskDefGet ~
    taskDefUpdate ~
    taskDefDelete ~
    taskDefList ~
    taskDefAdd
  }

  @Path("/{executorId}/taskdefs")
  @ApiOperation(value = "Get task definitions", nickname = "getTaskDefs",
    tags = Array("task definition"),
    httpMethod = "GET",
    response = classOf[Array[TaskDef]]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "executor id", required = true,
      dataType = "string", paramType = "path"
    ),
    new ApiImplicitParam(
      name = "assetClasses",
      value = "Comma-separated list of asset class IDs. If empty, all task definitions are reported.",
      required = false,
      dataType = "string", paramType = "query")
  ))
  def taskDefList: Route = {
    path("api" / "executors" / Segment / "taskdefs") { executorId ⇒
      parameters('assetClasses.?) { assetClassesOpt ⇒
        get {
          complete {
            val assetClasses = assetClassesOpt
              .map(_.split("\\s*,\\s*").toSet)
              .getOrElse(Set.empty)

            listTaskDefs(executorId, assetClasses)
          }
        }
      }
    }
  }

  @Path("/{executorId}/taskdefs")
  @ApiOperation(value = "Add a task definition", nickname = "addTaskDef",
    tags = Array("task definition"),
    httpMethod = "POST", code = 201,
    response = classOf[TaskDefAdd])
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "executor id", required = true,
      dataType = "string", paramType = "path"
    ),
    new ApiImplicitParam(
      name = "body", value = "task definition", required = true,
      dataTypeClass = classOf[TaskDefAdd], paramType = "body")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 409, message = "task definition already registered")
  ))
  def taskDefAdd: Route = {
    path("api" / "executors" / Segment / "taskdefs") { executorId ⇒
      (post & entity(as[TaskDefAdd])) { e ⇒
        complete {
          addTaskDef(executorId, e)
        }
      }
    }
  }

  @Path("/{executorId}/taskdefs/{taskDefId}")
  @ApiOperation(value = "Get a particular task definition", nickname = "getTaskDef",
    tags = Array("task definition"),
    httpMethod = "GET",
    response = classOf[TaskDef]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "executor id", required = true,
      dataType = "string", paramType = "path"
    ),
    new ApiImplicitParam(
      name = "taskDefId", value = "task definition id", required = true,
      dataType = "string", paramType = "path")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined task")
  ))
  def taskDefGet: Route = {
    path("api" / "executors" / Segment / "taskdefs" / Segment) { case (executorId, id) ⇒
      get {
        complete {
          getTaskDef(executorId, id)
        }
      }
    }
  }

  @Path("/{executorId}/taskdefs/{taskDefId}")
  @ApiOperation(value = "Update a task definition", nickname = "updateTaskDef",
    tags = Array("task definition"),
    httpMethod = "PUT",
    response = classOf[TaskDefUpdateResponse]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "executor id", required = true,
      dataType = "string", paramType = "path"
    ),
    new ApiImplicitParam(
      name = "taskDefId", value = "task definition id", required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "body", value = "Properties to update. All elements are optional.",
      required = true,
      dataTypeClass = classOf[TaskDefUpdate], paramType = "body")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined task")
  ))
  def taskDefUpdate: Route = {
    path("api" / "executors" / Segment / "taskdefs" / Segment) { case (executorId, id) ⇒
      (put & entity(as[TaskDefUpdate])) { e ⇒
        complete {
          updateTaskDef(executorId, id, e)
        }
      }
    }
  }

  @Path("/{executorId}/taskdefs/{taskDefId}")
  @ApiOperation(value = "Delete a task definition", nickname = "deleteTaskDef",
    tags = Array("task definition"),
    httpMethod = "DELETE",
    response = classOf[TaskDef]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "executor id", required = true,
      dataType = "string", paramType = "path"
    ),
    new ApiImplicitParam(
      name = "taskDefId", value = "task definition id", required = true,
      dataType = "string", paramType = "path")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined task")
  ))
  def taskDefDelete: Route = {
    path("api" / "executors" / Segment / "taskdefs" / Segment) { case (executorId, id) ⇒
      delete {
        complete {
          deleteTaskDef(executorId, id)
        }
      }
    }
  }

  @Path("/{executorId}/taskdefs/{taskDefId}/parameters")
  @ApiOperation(value = "Get task definition parameters", nickname = "getTaskDefParams",
    tags = Array("parameter"),
    httpMethod = "GET",
    response = classOf[Array[Parameter]]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "executor id", required = true,
      dataType = "string", paramType = "path"
    ),
    new ApiImplicitParam(
      name = "taskDefId", value = "Task definition ID.", required = true,
      dataType = "string", paramType = "query")
  ))
  def paramList: Route = {
    path("api" / "executors" / Segment / "taskdefs" / Segment / "parameters") {
      case (executorId, taskDefId) ⇒
        get {
          complete {
            listTaskDefParams(executorId, taskDefId)
          }
        }
    }
  }

  @Path("/{executorId}/taskdefs/{taskDefId}/parameters/{paramName}")
  @ApiOperation(value = "Get a task definition parameter", nickname = "getTaskDefParameter",
    tags = Array("parameter"),
    httpMethod = "GET", code = 200,
    response = classOf[Parameter])
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "executor id", required = true,
      dataType = "string", paramType = "path"
    ),
    new ApiImplicitParam(
      name = "taskDefId", value = "task definition id", required = true,
      dataType = "string", paramType = "path"
    ),
    new ApiImplicitParam(
      name = "paramName", value = "Parameter name", required = true,
      dataType = "string", paramType = "path"
    )
  ))
  def paramGet: Route = {
    path("api" / "executors" / Segment / "taskdefs" / Segment / "parameters" / Segment) {
      case (executorId, taskDefId, paramName) ⇒
        get {
          complete {
            getTaskDefParameter(executorId, taskDefId, paramName)
          }
        }
    }
  }

  @Path("/{executorId}/taskdefs/{taskDefId}/parameters")
  @ApiOperation(value = "Get a task definition parameter", nickname = "addTaskDefParameter",
    tags = Array("parameter"),
    httpMethod = "POST", code = 201,
    response = classOf[Parameter])
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "executor id", required = true,
      dataType = "string", paramType = "path"
    ),
    new ApiImplicitParam(
      name = "taskDefId", value = "task definition id", required = true,
      dataType = "string", paramType = "path"
    ),
    new ApiImplicitParam(
      name = "body", value = "parameter definition", required = true,
      dataTypeClass = classOf[Parameter], paramType = "body")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 409, message = "parameter already registered")
  ))
  def paramAdd: Route = {
    path("api" / "executors" / Segment / "taskdefs" / Segment / "parameters") {
      case (executorId, taskDefId) ⇒
      (post & entity(as[Parameter])) { p ⇒
        complete {
          addTaskDefParameter(executorId, taskDefId, p)
        }
      }
    }
  }

  @Path("/{executorId}/taskdefs/{taskDefId}/parameters/{paramName}")
  @ApiOperation(value = "Update a task definition parameter", nickname = "updateTaskDefParameter",
    tags = Array("parameter"),
    httpMethod = "PUT", code = 200,
    response = classOf[Parameter])
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "Executor id", required = true,
      dataType = "string", paramType = "path"
    ),
    new ApiImplicitParam(
      name = "taskDefId", value = "Task definition id", required = true,
      dataType = "string", paramType = "path"
    ),
    new ApiImplicitParam(
      name = "paramName", value = "Parameter name", required = true,
      dataType = "string", paramType = "path"
    ),
    new ApiImplicitParam(
      name = "body", value = "parameter attributes to update", required = true,
      dataTypeClass = classOf[ParameterUpdate], paramType = "body")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 409, message = "parameter already registered")
  ))
  def paramUpdate: Route = {
    path("api" / "executors" / Segment / "taskdefs" / Segment / "parameters" / Segment) {
      case (executorId, taskDefId, paramName) ⇒
      (put & entity(as[ParameterUpdate])) { u ⇒
        complete {
          updateTaskDefParameter(executorId, taskDefId, paramName, u)
        }
      }
    }
  }

  @Path("/{executorId}/taskdefs/{taskDefId}/parameters/{paramName}")
  @ApiOperation(value = "Delete a task definition parameter", nickname = "deleteTaskDefParameter",
    tags = Array("parameter"),
    httpMethod = "DELETE", code = 200,
    response = classOf[Parameter])
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "Executor id", required = true,
      dataType = "string", paramType = "path"
    ),
    new ApiImplicitParam(
      name = "taskDefId", value = "Task definition id", required = true,
      dataType = "string", paramType = "path"
    ),
    new ApiImplicitParam(
      name = "paramName", value = "Parameter name", required = true,
      dataType = "string", paramType = "path"
    )
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "parameter not found")
  ))
  def paramDelete: Route = {
    path("api" / "executors" / Segment / "taskdefs" / Segment / "parameters" / Segment) {
      case (executorId, taskDefId, paramName) ⇒
      delete {
        complete {
          deleteTaskDefParameter(executorId, taskDefId, paramName)
        }
      }
    }
  }

}
