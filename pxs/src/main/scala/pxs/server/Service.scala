package pxs.server

import akka.http.scaladsl.server.Route
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.cors


trait Service extends ExecutorService
      with AssetService with TaskDefService with PlanService {

  def allRoutes: Route = cors() {
    SwaggerSpecService.routes ~
    swaggerUi ~
    staticRoute ~
    assetRoute ~
    taskDefRoute ~
    executorRoute ~
    planRoute
  }

  private val staticRoute: Route = {
    get {
      getFromResourceDirectory("")
    }
  }

  private val swaggerUi: Route =
    path("apidoc") { getFromResource("swaggerui/index.html") } ~
      getFromResourceDirectory("swaggerui")
}
