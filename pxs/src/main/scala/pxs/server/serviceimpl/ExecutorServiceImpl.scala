package pxs.server.serviceimpl

import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model._
import com.typesafe.scalalogging.LazyLogging
import pxs.data.ExecutorRepo
import pxs.model._
import spray.json._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}


trait ExecutorServiceImpl extends BaseServiceImpl with LazyLogging {
  val executorRepo: ExecutorRepo

  def listExecutors: Future[ToResponseMarshallable] = {
    executorRepo.findAll map {
      case Right(m) ⇒
        m

      case Left(error) ⇒ errorResponse(error)
    }
  }

  def addExecutor(a: ExecutorAdd): Future[ToResponseMarshallable] = {
    executorRepo.insertOne(a) map {
      case Right(m) ⇒
        goodResponse(StatusCodes.Created, m.toJson)

      case Left(error) ⇒ errorResponse(error)
    }
  }

  def getExecutor(id: String): Future[ToResponseMarshallable] = {
    executorRepo.findById(id) map {
      case Right(m)  ⇒ m
      case Left(e)   ⇒ errorResponse(e)
    }
  }

  def getExecutorDetailed(id: String): Future[ToResponseMarshallable] = {
    executorRepo.findByIdDetailed(id) map {
      case Right(m)  ⇒ m
      case Left(e)   ⇒ errorResponse(e)
    }
  }

  def updateExecutor(executorId: String, u: ExecutorUpdate): Future[ToResponseMarshallable] = {
    executorRepo.updateOne(executorId, u) map {
      case Right(m)    ⇒ goodResponse(StatusCodes.OK, m.toJson)
      case Left(error) ⇒ errorResponse(error)
    }
  }

  def deleteExecutor(executorId: String): Future[ToResponseMarshallable] = {
    executorRepo.deleteOne(executorId) map {
      case Right(m)    ⇒ goodResponse(StatusCodes.OK, m.toJson)
      case Left(error) ⇒ errorResponse(error)
    }
  }

  def listAssets(executorId: String): Future[ToResponseMarshallable] = {
    executorRepo.assetRepo(executorId) map {
      case Right(ar)    ⇒
        ar.findAll map {
          case Right(m)    ⇒ goodResponse(StatusCodes.OK, m.toJson)
          case Left(error) ⇒ errorResponse(error)
        }
      case Left(error) ⇒ errorResponse(error)
    }
  }

  def addAsset(executorId: String, a: Asset): Future[ToResponseMarshallable] = {
    executorRepo.assetRepo(executorId) map {
      case Right(er)    ⇒
        er.insertOne(a) map {
          case Right(m)    ⇒ goodResponse(StatusCodes.Created, m.toJson)
          case Left(error) ⇒ errorResponse(error)
        }
      case Left(error) ⇒ errorResponse(error)
    }
  }

  def getAsset(executorId: String, assetId: String): Future[ToResponseMarshallable] = {
    executorRepo.assetRepo(executorId) map {
      case Right(ar)    ⇒
        ar.findById(assetId) map {
          case Right(m)    ⇒ goodResponse(StatusCodes.OK, m.toJson)
          case Left(error) ⇒ errorResponse(error)
        }
      case Left(error) ⇒ errorResponse(error)
    }
  }

  def updateAsset(executorId: String, assetId: String, u: AssetUpdate): Future[ToResponseMarshallable] = {
    executorRepo.assetRepo(executorId) map {
      case Right(er)    ⇒
        er.updateOne(assetId, u) map {
          case Right(m)    ⇒ goodResponse(StatusCodes.OK, m.toJson)
          case Left(error) ⇒ errorResponse(error)
        }
      case Left(error) ⇒ errorResponse(error)
    }
  }

  def deleteAsset(executorId: String, assetId: String): Future[ToResponseMarshallable] = {
    executorRepo.assetRepo(executorId) map {
      case Right(er)    ⇒
        er.deleteOne(assetId) map {
          case Right(m)    ⇒ goodResponse(StatusCodes.OK, m.toJson)
          case Left(error) ⇒ errorResponse(error)
        }
      case Left(error) ⇒ errorResponse(error)
    }
  }

  def listTaskDefs(executorId: String, assetClasses: Set[String]
                  ): Future[ToResponseMarshallable] = {
    executorRepo.taskDefRepo(executorId) map {
      case Right(taskDefRepo) ⇒
        taskDefRepo.findAll

      case Left(e) ⇒ Left(e)

    }
  }

  def addTaskDef(executorId: String, a: TaskDefAdd): Future[ToResponseMarshallable] = {
    executorRepo.taskDefRepo(executorId) map {
      case Right(taskDefRepo) ⇒
        taskDefRepo.insertOne(a) map {
          case Right(_) ⇒
            goodResponse(StatusCodes.Created, a.toJson)

          case Left(error) ⇒ errorResponse(error)
        }

      case Left(e) ⇒ Left(e)
    }
  }

  def getTaskDef(executorId: String, id: String
                ): Future[ToResponseMarshallable] = {
    executorRepo.taskDefRepo(executorId) flatMap {
      case Right(taskDefRepo) ⇒
        taskDefRepo.findById(id) flatMap {
          case Right(taskDef) ⇒ Future.successful(taskDef)
          case Left(error) ⇒ Future.successful(errorResponse(error))
        }

      case Left(error) ⇒ Future.successful(errorResponse(error))
    }
  }

  def updateTaskDef(executorId: String, id: String, u: TaskDefUpdate
                   ): Future[ToResponseMarshallable] = {
    val promise = Promise[ToResponseMarshallable]
    executorRepo.taskDefRepo(executorId) map {
      case Right(taskDefRepo) ⇒
        taskDefRepo.updateOne(id, u) map {
          case Right(_)    ⇒ promise success TaskDefUpdateResponse(id)
          case Left(error) ⇒ promise success errorResponse(error)
        }

      case Left(e) ⇒ Left(e)
    }
    promise.future
  }

  def deleteTaskDef(executorId: String, id: String
                   ): Future[ToResponseMarshallable] = {
    val promise = Promise[ToResponseMarshallable]
    executorRepo.taskDefRepo(executorId) map {
      case Right(taskDefRepo) ⇒
        taskDefRepo.deleteOne(id) map {
          case Right(m)    ⇒ promise success goodResponse(StatusCodes.OK, m.toJson)
          case Left(error) ⇒ promise success errorResponse(error)
        }

      case Left(e) ⇒ Left(e)
    }
    promise.future
  }

  def listTaskDefParams(executorId: String, taskDefId: String
                       ): Future[ToResponseMarshallable] = {
    val promise = Promise[ToResponseMarshallable]
    executorRepo.taskDefRepo(executorId) map {
      case Right(taskDefRepo) ⇒
        taskDefRepo.listParams(taskDefId) map {
          case Right(ps) ⇒ promise success goodResponse(StatusCodes.OK, ps.toJson)
          case Left(error) ⇒ promise success errorResponse(error)
        }

      case Left(e) ⇒ Left(e)
    }
    promise.future
  }

  def getTaskDefParameter(executorId: String, taskDefId: String, paramName: String
                         ): Future[ToResponseMarshallable] = {
    val promise = Promise[ToResponseMarshallable]
    executorRepo.taskDefRepo(executorId) map {
      case Right(taskDefRepo) ⇒
        taskDefRepo.getParam(taskDefId, paramName) map {
          case Right(p) ⇒ promise success goodResponse(StatusCodes.OK, p.toJson)
          case Left(error) ⇒ promise success errorResponse(error)
        }

      case Left(e) ⇒ Left(e)
    }
    promise.future
  }

  def addTaskDefParameter(executorId: String, taskDefId: String, a: Parameter
                         ): Future[ToResponseMarshallable] = {
    val promise = Promise[ToResponseMarshallable]
    executorRepo.taskDefRepo(executorId) map {
      case Right(taskDefRepo) ⇒
        taskDefRepo.addParam(taskDefId, a) map {
          case Right(p) ⇒
            promise success goodResponse(StatusCodes.Created, p.toJson)

          case Left(error) ⇒ promise success errorResponse(error)
        }

      case Left(e) ⇒ Left(e)
    }
    promise.future
  }

  def updateTaskDefParameter(executorId: String, taskDefId: String, paramName: String, u: ParameterUpdate
                         ): Future[ToResponseMarshallable] = {
    val promise = Promise[ToResponseMarshallable]
    executorRepo.taskDefRepo(executorId) map {
      case Right(taskDefRepo) ⇒
        taskDefRepo.updateParam(taskDefId, paramName, u) map {
          case Right(p) ⇒
            promise success goodResponse(StatusCodes.OK, p.toJson)

          case Left(error) ⇒ promise success errorResponse(error)
        }

      case Left(e) ⇒ Left(e)
    }
    promise.future
  }

  def deleteTaskDefParameter(executorId: String, taskDefId: String, paramName: String
                         ): Future[ToResponseMarshallable] = {
    val promise = Promise[ToResponseMarshallable]
    executorRepo.taskDefRepo(executorId) map {
      case Right(taskDefRepo) ⇒
        taskDefRepo.deleteParam(taskDefId, paramName) map {
          case Right(p) ⇒
            promise success goodResponse(StatusCodes.OK, p.toJson)

          case Left(error) ⇒ promise success errorResponse(error)
        }

      case Left(e) ⇒ Left(e)
    }
    promise.future
  }

}
