package pxs.server.serviceimpl

import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model._
import com.typesafe.scalalogging.LazyLogging
import pxs.data.PlanRepo
import pxs.model._
import spray.json._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}


trait PlanServiceImpl extends BaseServiceImpl with LazyLogging {
  val planRepo: PlanRepo

  def listPlans(assetIds: Set[String]): Future[Seq[Plan]] = {
    if (assetIds.nonEmpty)
      planRepo.findForAssets(assetIds)
    else
      planRepo.findAll
  }

  def addPlan(a: PlanAdd): Future[ToResponseMarshallable] = {
    logger.debug(s"addPlan: $a")
    planRepo.insertOne(a) map {
      case Right(m) ⇒
        val r = PlanAddResponse(
          planId = m.planId,
          name = m.name
        )
        goodResponse(StatusCodes.Created, r.toJson)

      case Left(error) ⇒ errorResponse(error)
    }
  }

  def getPlan(id: String): Future[ToResponseMarshallable] = {
    logger.debug(s"getPlan: id=$id")
    planRepo.findById(id) map {
      case Right(m)  ⇒ m
      case Left(e)   ⇒ errorResponse(e)
    }
  }

  def updatePlan(id: String, u: PlanUpdate): Future[ToResponseMarshallable] = {
    logger.debug(s"updatePlan: $u")
    planRepo.updateOne(id, u) map {
      case Right(_)    ⇒ PlanUpdateResponse(id)
      case Left(error) ⇒ errorResponse(error)
    }
  }

  def deletePlan(id: String): Future[ToResponseMarshallable] = {
    logger.debug(s"deletePlan: id=$id")
    planRepo.deleteOne(id) map {
      case Right(m) ⇒
        goodResponse(StatusCodes.OK, m.toJson)
      case Left(error) ⇒ errorResponse(error)
    }
  }

  //
  // Tasks
  //

  def listTasks(planId: String): Future[ToResponseMarshallable] = {
    planRepo.listTasks(planId) map {
      case Right(ts)  ⇒ ts
      case Left(e)   ⇒ errorResponse(e)
    }
  }

  def getTask(planId: String, taskId: String): Future[ToResponseMarshallable] = {
    planRepo.getTask(planId, taskId) map {
      case Right(m)  ⇒ m
      case Left(e)   ⇒ errorResponse(e)
    }
  }

  def addTask(planId: String, a: TaskAdd): Future[ToResponseMarshallable] = {
    logger.debug(s"addTask: $a")
    val promise = Promise[ToResponseMarshallable]()

    planRepo.insertTask(planId, a) map {
      case Right(t)  ⇒
        val r = TaskAddResponse(
          planId = planId,
          taskId = t.taskId
        )
        promise success goodResponse(StatusCodes.Created, r.toJson)

      case Left(e)   ⇒ promise success errorResponse(e)
    }
    promise.future
  }

  def deleteTask(planId: String, taskId: String): Future[ToResponseMarshallable] = {
    logger.debug(s"deleteTask: $taskId")
    val promise = Promise[ToResponseMarshallable]()
    planRepo.deleteTask(planId, taskId) map {
      case Right(m)  ⇒
        val r = TaskDeleteResponse(
          planId = planId,
          taskId = m.taskId
        )
        promise success goodResponse(StatusCodes.OK, r.toJson)

      case Left(e)   ⇒ errorResponse(e)
    }
    promise.future
  }

  def updateTask(planId: String, taskId: String, u: TaskUpdate): Future[ToResponseMarshallable] = {
    logger.debug(s"updateTask: $u")
    val promise = Promise[ToResponseMarshallable]()
    planRepo.updateTask(planId, taskId, u) map {
      case Right(t)  ⇒
        val r = TaskAddResponse(
          planId = planId,
          taskId = t.taskId
        )
        promise success goodResponse(StatusCodes.OK, r.toJson)

      case Left(e)   ⇒ errorResponse(e)
    }
    promise.future
  }

  //
  // Arguments
  //

  def listArguments(planId: String, taskId: String, namesOpt: Option[String]
                   ): Future[ToResponseMarshallable] = {
    planRepo.listArgs(planId, taskId) map {
      case Right(as) ⇒ as
      case Left(e)   ⇒ errorResponse(e)
    }
  }

  def getArgument(planId: String, taskId: String, paramName: String
                   ): Future[ToResponseMarshallable] = {
    planRepo.getArg(planId, taskId, paramName) map {
      case Right(a)  ⇒ a
      case Left(e)   ⇒ errorResponse(e)
    }
  }

  def addArgument(planId: String, taskId: String, a: Argument
                 ): Future[ToResponseMarshallable] = {
    //println(fansi.Color.LightMagenta(s"addArgument: a=$a"))
    planRepo.insertArg(planId, taskId, a) map {
      case Right(m)  ⇒ goodResponse(StatusCodes.Created, m.toJson)
      case Left(e)   ⇒ errorResponse(e)
    }
  }

  def updateArgument(planId: String, taskId: String, paramName: String,
                     u: ArgumentUpdate
                   ): Future[ToResponseMarshallable] = {
    planRepo.updateArg(planId, taskId, paramName, u) map {
      case Right(a)  ⇒ a
      case Left(e)   ⇒ errorResponse(e)
    }
  }

  def deleteArgument(planId: String, taskId: String, paramName: String
                   ): Future[ToResponseMarshallable] = {
    planRepo.deleteArg(planId, taskId, paramName) map {
      case Right(m)  ⇒ m
      case Left(e)   ⇒ errorResponse(e)
    }
  }

}
