package pxs.server.serviceimpl

import akka.http.scaladsl.model._
import pxs.model.{GnError, PxsJsonImplicits}
import spray.json._


trait BaseServiceImpl extends PxsJsonImplicits {
  def goodResponse(status: StatusCode, jsonValue: JsValue): HttpResponse = {
    HttpResponse(
      status = status,
      entity = HttpEntity(
        ContentType(MediaTypes.`application/json`),
        jsonValue.compactPrint
      )
    )
  }

  def errorResponse(error: GnError): HttpResponse = {
    HttpResponse(
      status = error.code,
      entity = HttpEntity(
        ContentType(MediaTypes.`application/json`),
        error.toJson.compactPrint
      )
    )
  }
}
