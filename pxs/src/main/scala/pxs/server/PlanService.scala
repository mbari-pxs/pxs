package pxs.server

import javax.ws.rs.Path

import akka.http.scaladsl.server.Route
import io.swagger.annotations._
import pxs.model._
import pxs.server.serviceimpl.PlanServiceImpl

@Path("/plans")
trait PlanService extends PlanServiceImpl with BaseService {
  def planRoute: Route = {
    argGet ~
    argList ~
    argUpdate ~
    argDelete ~
    argAdd ~
    taskGet ~
    taskList ~
    taskUpdate ~
    taskDelete ~
    taskAdd ~
    planList ~
    planAdd ~
    planGet ~
    planUpdate ~
    planDelete
  }

  @ApiOperation(value = "Get plans", nickname = "getPlans",
    tags = Array("plan"),
    httpMethod = "GET",
    response = classOf[Array[Plan]]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "assets",
      value = "Comma-separated list of asset IDs. If empty, all plans are reported.",
      required = false,
      dataType = "string", paramType = "query")
  ))
  def planList: Route = {
    path("api" / "plans") {
      parameters('assets.?) { assetsOpt ⇒
        get {
          complete {
            val assetIds = assetsOpt
              .map(_.split("\\s*,\\s*").toSet)
              .getOrElse(Set.empty)

            listPlans(assetIds)
          }
        }
      }
    }
  }

  @ApiOperation(value = "Add a plan", nickname = "addPlan",
    tags = Array("plan"),
    httpMethod = "POST", code = 201,
    response = classOf[PlanAddResponse])
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "body", value = "plan definition", required = true,
      dataTypeClass = classOf[PlanAdd], paramType = "body")
  ))
  //@ApiResponses(Array(
  //  new ApiResponse(code = 409, message = "Plan already registered")
  //))
  def planAdd: Route = path("api" / "plans") {
    (post & entity(as[PlanAdd])) { pla ⇒
      complete {
        addPlan(pla)
      }
    }
  }

  @Path("/{planId}")
  @ApiOperation(value = "Get a particular plan", nickname = "getPlan",
    tags = Array("plan"),
    httpMethod = "GET",
    response = classOf[Plan]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "planId", value = "plan id", required = true,
      dataType = "string", paramType = "path")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined plan")
  ))
  def planGet: Route = path("api" / "plans" / Segment) { plid ⇒
    get {
      complete {
        getPlan(plid)
      }
    }
  }

  @Path("/{planId}")
  @ApiOperation(value = "Update a plan", nickname = "updatePlan",
    tags = Array("plan"),
    httpMethod = "PUT",
    response = classOf[PlanUpdateResponse]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "planId", value = "ID of plan to be updated", required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "body", value = "Properties to update. All elements are optional.",
      required = true,
      dataTypeClass = classOf[PlanUpdate], paramType = "body")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined plan")
  ))
  def planUpdate: Route = path("api" / "plans" / Segment) { plid ⇒
    (put & entity(as[PlanUpdate])) { pla ⇒
      complete {
        updatePlan(plid, pla)
      }
    }
  }

  @Path("/{planId}")
  @ApiOperation(value = "Delete a plan", nickname = "deletePlan",
    tags = Array("plan"),
    httpMethod = "DELETE",
    response = classOf[Plan]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "planId", value = "ID of plan to be removed", required = true,
      dataType = "string", paramType = "path")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined plan")
  ))
  def planDelete: Route = path("api" / "plans" / Segment) { plid ⇒
    delete {
      complete {
        deletePlan(plid)
      }
    }
  }

  //
  // Tasks
  //

  @Path("/{planId}/tasks")
  @ApiOperation(value = "Get the tasks in a plan", nickname = "getTasks",
    tags = Array("task"),
    httpMethod = "GET",
    response = classOf[Array[Task]]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "planId",
      value = "Plan ID.",
      required = true,
      dataType = "string", paramType = "path")
  ))
  def taskList: Route = {
    path("api" / "plans" / Segment / "tasks") { planId ⇒
      get {
        complete {
          listTasks(planId)
        }
      }
    }
  }

  @Path("/{planId}/tasks/{taskId}")
  @ApiOperation(value = "Get a task in a plan", nickname = "getTask",
    tags = Array("task"),
    httpMethod = "GET",
    response = classOf[Task]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "planId",
      value = "Plan ID.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "taskId",
      value = "task ID.",
      required = true,
      dataType = "string", paramType = "path")
  ))
  def taskGet: Route = {
    path("api" / "plans" / Segment / "tasks" / Segment) { case (planId, taskId) ⇒
      get {
        complete {
          getTask(planId, taskId)
        }
      }
    }
  }

  @Path("/{planId}/tasks")
  @ApiOperation(value = "Add a task to a plan", nickname = "addTask",
    tags = Array("task"),
    httpMethod = "POST",
    response = classOf[TaskAddResponse]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "planId",
      value = "Plan ID.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "body", value = "task definition", required = true,
      dataTypeClass = classOf[TaskAdd], paramType = "body")
  ))
  def taskAdd: Route = {
    path("api" / "plans" / Segment / "tasks") { planId ⇒
      (post & entity(as[TaskAdd])) { sta ⇒
        complete {
          addTask(planId, sta)
        }
      }
    }
  }

  @Path("/{planId}/tasks/{taskId}")
  @ApiOperation(value = "Delete a task in a plan", nickname = "deleteTask",
    tags = Array("task"),
    httpMethod = "DELETE",
    response = classOf[TaskDeleteResponse]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "planId",
      value = "Plan ID.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "taskId",
      value = "Task ID.",
      required = true,
      dataType = "string", paramType = "path")
  ))
  def taskDelete: Route = {
    path("api" / "plans" / Segment / "tasks" / Segment) { case (planId, taskId) ⇒
      complete {
        deleteTask(planId, taskId)
      }
    }
  }

  @Path("/{planId}/tasks/{taskId}")
  @ApiOperation(value = "Update a task in a plan", nickname = "updateTask",
    tags = Array("task"),
    httpMethod = "PUT",
    response = classOf[TaskUpdateResponse]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "planId",
      value = "Plan ID.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "taskId",
      value = "Task ID.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "body", value = "Attributes that can be updated in a task", required = true,
      dataTypeClass = classOf[TaskUpdate], paramType = "body")
  ))
  def taskUpdate: Route = {
    path("api" / "plans" / Segment / "tasks" / Segment) { case (planId, taskId) ⇒
      (put & entity(as[TaskUpdate])) { stu ⇒
        complete {
          updateTask(planId, taskId, stu)
        }
      }
    }
  }

  //
  // Arguments
  //

  @Path("/{planId}/tasks/{taskId}/arguments")
  @ApiOperation(value = "Get the arguments in a task", nickname = "getArguments",
    tags = Array("argument"),
    httpMethod = "GET",
    response = classOf[Array[Argument]]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "planId",
      value = "Plan ID.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "taskId",
      value = "Task ID.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "names",
      value = "Comma-separated list of parameter names (optional).",
      required = false,
      dataType = "string", paramType = "query")
  ))
  def argList: Route = {
    path("api" / "plans" / Segment / "tasks" / Segment / "arguments") {
      case (planId, taskId) ⇒
        parameters('names.?) { namesOpt ⇒
          get {
            complete {
              listArguments(planId, taskId, namesOpt)
            }
          }
        }
    }
  }

  @Path("/{planId}/tasks/{taskId}/arguments/{paramName}")
  @ApiOperation(value = "Get an argument in a task", nickname = "getArgument",
    tags = Array("argument"),
    httpMethod = "GET",
    response = classOf[Argument]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "planId",
      value = "Plan ID.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "taskId",
      value = "Task ID.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "paramName",
      value = "Parameter name.",
      required = true,
      dataType = "string", paramType = "path"),
  ))
  def argGet: Route = {
    path("api" / "plans" / Segment / "tasks" / Segment / "arguments" / Segment) {
      case (planId, taskId, paramName) ⇒
        get {
          complete {
            getArgument(planId, taskId, paramName)
          }
        }
    }
  }

  @Path("/{planId}/tasks/{taskId}/arguments")
  @ApiOperation(value = "Add an argument to a task", nickname = "addArgument",
    tags = Array("argument"),
    httpMethod = "POST",
    response = classOf[Argument]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "planId",
      value = "Plan ID.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "taskId",
      value = "Task ID.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "body", value = "Argument information", required = true,
      dataTypeClass = classOf[Argument], paramType = "body")
  ))
  def argAdd: Route = {
    path("api" / "plans" / Segment / "tasks" / Segment / "arguments") {
      case (planId, taskId) ⇒
        (post & entity(as[Argument])) { a ⇒
          complete {
            addArgument(planId, taskId, a)
          }
        }
    }
  }

  @Path("/{planId}/tasks/{taskId}/arguments/{paramName}")
  @ApiOperation(value = "Update an argument in a task", nickname = "updateArgument",
    tags = Array("argument"),
    httpMethod = "PUT",
    response = classOf[Argument]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "planId",
      value = "Plan ID.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "taskId",
      value = "Task ID.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "paramName",
      value = "Parameter name.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "body", value = "New value for the parameter in this argument", required = true,
      dataTypeClass = classOf[ArgumentUpdate], paramType = "body")
  ))
  def argUpdate: Route = {
    path("api" / "plans" / Segment / "tasks" / Segment / "arguments" / Segment) {
      case (planId, taskId, paramName) ⇒
        (put & entity(as[ArgumentUpdate])) { u ⇒
          complete {
            updateArgument(planId, taskId, paramName, u)
          }
        }
    }
  }

  @Path("/{planId}/tasks/{taskId}/arguments/{paramName}")
  @ApiOperation(value = "Delete an argument in a task", nickname = "deleteArgument",
    tags = Array("argument"),
    httpMethod = "DELETE",
    response = classOf[Argument]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "planId",
      value = "Plan ID.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "taskId",
      value = "Task ID.",
      required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "paramName",
      value = "Parameter name.",
      required = true,
      dataType = "string", paramType = "path"),
  ))
  def argDelete: Route = {
    path("api" / "plans" / Segment / "tasks" / Segment / "arguments" / Segment) {
      case (planId, taskId, paramName) ⇒
        delete {
          complete {
            deleteArgument(planId, taskId, paramName)
          }
        }
    }
  }

}
