package pxs.server

import akka.http.scaladsl.server.Route
import io.swagger.annotations._
import javax.ws.rs.Path
import pxs.model._
import pxs.server.serviceimpl.ExecutorServiceImpl

@Path("/executors")
trait AssetService extends ExecutorServiceImpl with BaseService {
  def assetRoute: Route = {
    assetGet ~
    assetList ~
    assetAdd ~
    assetUpdate ~
    assetDelete
  }

  @Path("/{executorId}/assets")
  @ApiOperation(value = "Get all assets of an Executor", nickname = "getAssets",
    tags = Array("asset"),
    httpMethod = "GET",
    response = classOf[Array[Asset]]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "executor id", required = true,
      dataType = "string", paramType = "path")
  ))
  def assetList: Route = {
    path("api" / "executors" / Segment / "assets") { executorId ⇒
      get {
        complete {
          listAssets(executorId)
        }
      }
    }
  }

  @Path("/{executorId}/assets")
  @ApiOperation(value = "Add an asset", nickname = "addAsset",
    tags = Array("asset"),
    httpMethod = "POST", code = 201,
    response = classOf[Asset])
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "Executor id", required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "body", value = "asset definition", required = true,
      dataTypeClass = classOf[Asset], paramType = "body")
  ))
  //@ApiResponses(Array(
  //  new ApiResponse(code = 409, message = "Asset already registered")
  //))
  def assetAdd: Route = {
    path("api" / "executors" / Segment / "assets") { executorId ⇒
      (post & entity(as[Asset])) { pla ⇒
        complete {
          addAsset(executorId, pla)
        }
      }
    }
  }

  @Path("/{executorId}/assets/{assetId}")
  @ApiOperation(value = "Get a particular asset", nickname = "getAsset",
    tags = Array("asset"),
    httpMethod = "GET",
    response = classOf[Asset]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "Executor id", required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "assetId", value = "asset id", required = true,
      dataType = "string", paramType = "path")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined asset")
  ))
  def assetGet: Route = {
    path("api" / "executors" / Segment / "assets" / Segment) { case (executorId, assetId) ⇒
      get {
        complete {
          getAsset(executorId, assetId)
        }
      }
    }
  }

  @Path("/{executorId}/assets/{assetId}")
  @ApiOperation(value = "Update an asset", nickname = "updateAsset",
    tags = Array("asset"),
    httpMethod = "PUT",
    response = classOf[Asset]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "Executor id", required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "assetId", value = "asset id", required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "body", value = "Properties to update. All elements are optional.",
      required = true,
      dataTypeClass = classOf[AssetUpdate], paramType = "body")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined asset")
  ))
  def assetUpdate: Route = {
    path("api" / "executors" / Segment / "assets" / Segment) { case (executorId, assetId) ⇒
      (put & entity(as[AssetUpdate])) { pla ⇒
        complete {
          updateAsset(executorId, assetId, pla)
        }
      }
    }
  }

  @Path("/{executorId}/assets/{assetId}")
  @ApiOperation(value = "Delete an asset", nickname = "deleteAsset",
    tags = Array("asset"),
    httpMethod = "DELETE",
    response = classOf[Asset]
  )
  @ApiImplicitParams(Array(
    new ApiImplicitParam(
      name = "executorId", value = "Executor id", required = true,
      dataType = "string", paramType = "path"),
    new ApiImplicitParam(
      name = "assetId", value = "asset id", required = true,
      dataType = "string", paramType = "path")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 404, message = "Undefined asset")
  ))
  def assetDelete: Route = {
    path("api" / "executors" / Segment / "assets" / Segment) { case (executorId, assetId) ⇒
      delete {
        complete {
          deleteAsset(executorId, assetId)
        }
      }
    }
  }
}
