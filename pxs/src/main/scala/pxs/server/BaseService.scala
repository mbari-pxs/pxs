package pxs.server

import akka.http.scaladsl.server.Directives
import io.swagger.annotations._
import pxs.model.PxsJsonImplicits

@Api(produces = "application/json")
trait BaseService extends PxsJsonImplicits with Directives {
}
