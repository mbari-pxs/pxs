package pxs.server

import com.github.swagger.akka.SwaggerHttpService
import com.github.swagger.akka.model.Info
import pxs.config.cfg

object SwaggerSpecService extends SwaggerHttpService {
  override val info = Info(
    version = cfg.pxs.version,
    title = "PXS API",
    description =
      """## PXS - Planning Execution Service
        |
        |PXS is intended to be a central service for definition,
        |storage, maintenance, and execution of vehicle missions.
        |
        | **NOTE**: *Under construction*
        | - Current focus is on data model and API specification
        | - Preliminary implementation
        |
        |See [https://github.com/mbari-pxs](https://github.com/mbari-pxs)
      """.stripMargin
  )

  // the url of your api, not swagger's json endpoint.
  // Note that no scheme should be included.
  override val host: String = cfg.externalUrl.replaceFirst("^https?://", "")

  override val basePath: String = "/api"    //the basePath for the API you are exposing

  override val apiDocsPath: String = "apidoc" //where you want the swagger-json endpoint exposed

  override val apiClasses: Set[Class[_]] = Set(
    classOf[ExecutorService],
    classOf[AssetService],
    classOf[PlanService],
    classOf[TaskDefService]
  )
}
