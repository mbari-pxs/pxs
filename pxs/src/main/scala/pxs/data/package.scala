package pxs

import java.util.UUID

import pxs.model._

package object data {
  def tda2td(a: TaskDefAdd): TaskDef =
    TaskDef(
      taskDefId = a.taskDefId,
      assetClasses = a.assetClasses,
      description = a.description
    )

  def paramUpdate2param(p: Parameter, u: ParameterUpdate): Parameter = {
    var n = p
    u.`type` foreach {x ⇒  n = n.copy(`type` = x)}
    u.required foreach {x ⇒  n = n.copy(required = x)}
    u.defaultValue foreach {x ⇒  n = n.copy(defaultValue = Some(x))}
    u.description foreach {x ⇒  n = n.copy(description = Some(x))}
    n
  }

  def ta2task(ta: TaskAdd): Task =
    Task(
      taskId = randomId,
      taskDefId = ta.taskDefId,
      arguments = ta.arguments.getOrElse(Seq.empty),
      assetId = ta.assetId,
      name = ta.name,
      description = ta.description,
      start = ta.start,
      end = ta.end,
      // TODO geometry
    )

  def tu2task(ta: Task, tu: TaskUpdate): Task = {
    var n = ta
    tu.taskDefId foreach {x ⇒ n = n.copy(taskId = x)}
    tu.arguments foreach {x ⇒ n = n.copy(arguments = x)}
    tu.assetId   foreach {x ⇒ n = n.copy(assetId = x)}
    tu.name      foreach {x ⇒ n = n.copy(name = Some(x))}
    tu.description foreach {x ⇒ n = n.copy(description = Some(x))}
    tu.start     foreach {x ⇒ n = n.copy(start = Some(x))}
    tu.end       foreach {x ⇒ n = n.copy(end = Some(x))}
    // TODO geometry
    n
  }

  def argu2arg(m: Argument, u: ArgumentUpdate): Argument = {
    m.copy(paramValue = u.paramValue)
  }

  def randomId: String = UUID.randomUUID().toString
}
