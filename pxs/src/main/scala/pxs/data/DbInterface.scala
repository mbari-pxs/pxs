package pxs.data

import pxs.model._

import scala.concurrent.Future

trait DbInterface {

  def details: String

  def executorRepo: ExecutorRepo

  def planRepo: PlanRepo

  def clearData(): Unit

  def flush(): Unit

  def close(): Unit
}

trait ExecutorRepo {
  def findAll: Future[Either[GnError, Seq[ExecutorGetResponse]]]

  def insertOne(a: ExecutorAdd): Future[Either[GnError, ExecutorAddResponse]]

  def findById(id: String): Future[Either[GnError, ExecutorGetResponse]]

  def findByIdDetailed(id: String): Future[Either[GnError, Executor]]

  def updateOne(id: String, u: ExecutorUpdate): Future[Either[GnError, ExecutorGetResponse]]

  def deleteOne(id: String): Future[Either[GnError, ExecutorGetResponse]]

  def removeAll(): Future[Either[GnError, String]]

  def assetRepo(id: String): Future[Either[GnError, AssetRepo]]

  def taskDefRepo(id: String): Future[Either[GnError, TaskDefRepo]]
}

trait AssetRepo {
  def findAll: Future[Either[GnError, Seq[Asset]]]

  def insertOne(a: Asset): Future[Either[GnError, Asset]]

  def findById(id: String): Future[Either[GnError, Asset]]

  def updateOne(id: String, u: AssetUpdate): Future[Either[GnError, AssetUpdateResponse]]

  def deleteOne(id: String): Future[Either[GnError, Asset]]

  def removeAll(): Future[Either[GnError, String]]

}

trait TaskDefRepo {
  def findAll: Future[Either[GnError, Seq[TaskDef]]]

  // TODO this request should be for Executor resource
  //def findForAssetClasses(assetClasses: Set[String]): Future[Either[GnError, Seq[TaskDef]]]

  def insertOne(a: TaskDefAdd): Future[Either[GnError, TaskDef]]

  def findById(id: String): Future[Either[GnError, TaskDef]]

  def updateOne(id: String, u: TaskDefUpdate): Future[Either[GnError, String]]

  def deleteOne(id: String): Future[Either[GnError, TaskDef]]

  def removeAll(): Future[Either[GnError, String]]

  def listParams(id: String): Future[Either[GnError, Seq[Parameter]]]

  def getParam(id: String, name: String): Future[Either[GnError, Parameter]]

  def addParam(id: String, p: Parameter): Future[Either[GnError, Parameter]]

  def updateParam(id: String, paramName: String, p: ParameterUpdate): Future[Either[GnError, Parameter]]

  def deleteParam(id: String, paramName: String): Future[Either[GnError, Parameter]]

}

trait PlanRepo {
  def findAll: Future[Seq[Plan]]

  def findForAssets(assetIds: Set[String]): Future[Seq[Plan]]

  def insertOne(a: PlanAdd): Future[Either[GnError, Plan]]

  def findById(id: String): Future[Either[GnError, Plan]]

  def updateOne(id: String, u: PlanUpdate): Future[Either[GnError, String]]

  def deleteOne(id: String): Future[Either[GnError, Plan]]

  def removeAll(): Future[String]

  def listTasks(id: String): Future[Either[GnError, Seq[Task]]]

  def getTask(id: String, taskId: String): Future[Either[GnError, Task]]

  def insertTask(id: String, a: TaskAdd): Future[Either[GnError, Task]]

  def deleteTask(planId: String, taskId: String): Future[Either[GnError, Task]]

  def updateTask(id: String, taskId: String, u: TaskUpdate): Future[Either[GnError, Task]]

  def listArgs(id: String, taskId: String): Future[Either[GnError, Seq[Argument]]]

  def getArg(id: String, taskId: String, paramName: String): Future[Either[GnError, Argument]]

  def insertArg(id: String, taskId: String, a: Argument): Future[Either[GnError, Argument]]

  def deleteArg(planId: String, taskId: String, paramName: String): Future[Either[GnError, Argument]]

  def updateArg(id: String, taskId: String, paramName: String, u: ArgumentUpdate): Future[Either[GnError, Argument]]

}
