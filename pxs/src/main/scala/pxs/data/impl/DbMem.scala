package pxs.data.impl

import java.io.File
import java.nio.charset.StandardCharsets
import java.nio.file.Files

import com.typesafe.scalalogging.{LazyLogging ⇒ Logging}
import pxs.data._
import pxs.model._
import spray.json._

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}


class DbMem(testing: Boolean = false)
           (implicit ec: ExecutionContext) extends DbInterface with Logging {

  def details: String = "DbMem"

  lazy val executorRepo: ExecutorRepo = new ExecutorRepo() {
    def findAll: Future[Either[GnError, Seq[ExecutorGetResponse]]] = Future {
      executors.findAll
    }

    def insertOne(a: ExecutorAdd): Future[Either[GnError, ExecutorAddResponse]] = Future {
      executors.insertOne(a)
    }

    def findById(id: String): Future[Either[GnError, ExecutorGetResponse]] = Future {
      executors.findById(id)
    }

    def findByIdDetailed(id: String): Future[Either[GnError, Executor]] = Future {
      executors.findByIdDetailed(id)
    }

    def updateOne(id: String, u: ExecutorUpdate): Future[Either[GnError, ExecutorGetResponse]] = Future {
      executors.updateOne(id, u)
    }

    def deleteOne(id: String): Future[Either[GnError, ExecutorGetResponse]] = Future {
      executors.deleteOne(id)
    }

    def removeAll(): Future[Either[GnError, String]] = Future {
      executors.removeAll()
    }

    def taskDefRepo(id: String): Future[Either[GnError, TaskDefRepo]] = Future {
      Right(new ExecutorTaskDefRepo(id))
    }


    def assetRepo(id: String): Future[Either[GnError, AssetRepo]] = Future {
      executors.findById(id) match {
        case Right(_) ⇒
          Right(assetRepos.getOrElse(id, {
            val r = new XAssetRepo(id)
            assetRepos.update(id, r)
            r
          }))

        case Left(e) ⇒ Left(e)
      }
    }

    private val assetRepos: mutable.Map[String, AssetRepo] = mutable.Map.empty

    class XAssetRepo(executorId: String) extends AssetRepo {
      def findAll: Future[Either[GnError, Seq[Asset]]] = Future {
        executors.assets.findAll(executorId)
      }

      def insertOne(a: Asset): Future[Either[GnError, Asset]] = Future {
        executors.assets.insertOne(executorId, a)
      }

      def findById(id: String): Future[Either[GnError, Asset]] = Future {
        executors.assets.findById(executorId, id)
      }

      def updateOne(id: String, u: AssetUpdate): Future[Either[GnError, AssetUpdateResponse]] = Future {
        executors.assets.updateOne(executorId, id, u)
      }

      def deleteOne(id: String): Future[Either[GnError, Asset]] = Future {
        executors.assets.deleteOne(executorId, id)
      }

      def removeAll(): Future[Either[GnError, String]] = Future {
        executors.assets.removeAll(executorId)
      }
    }

    class ExecutorTaskDefRepo(executorId: String) extends TaskDefRepo {

      def findAll: Future[Either[GnError, Seq[TaskDef]]] = Future {
        executors.taskDefs.findAll(executorId)
      }

      def updateOne(id: String, u: TaskDefUpdate): Future[Either[GnError, String]] = Future {
        executors.taskDefs.updateOne(executorId, id, u)
      }

      def findById(id: String): Future[Either[GnError, TaskDef]] = Future {
        executors.taskDefs.findById(executorId, id)
      }

      def insertOne(a: TaskDefAdd): Future[Either[GnError, TaskDef]] = Future {
        executors.taskDefs.insertOne(executorId, a)
      }

      def deleteOne(id: String): Future[Either[GnError, TaskDef]] = Future {
        executors.taskDefs.deleteOne(executorId, id)
      }

      def removeAll(): Future[Either[GnError, String]] = Future {
        executors.taskDefs.removeAll(executorId)
      }

      def listParams(id: String): Future[Either[GnError, Seq[Parameter]]] = Future {
        executors.taskDefs.listParams(executorId, id)
      }

      def getParam(id: String, name: String): Future[Either[GnError, Parameter]] = Future {
        executors.taskDefs.getParam(executorId, id, name)
      }

      def addParam(id: String, p: Parameter): Future[Either[GnError, Parameter]] = Future {
        executors.taskDefs.addParam(executorId, id, p)
      }

      def updateParam(id: String, paramName: String, u: ParameterUpdate
                     ): Future[Either[GnError, Parameter]] = Future {
        executors.taskDefs.updateParam(executorId, id, paramName, u)
      }

      def deleteParam(id: String, paramName: String
                     ): Future[Either[GnError, Parameter]] = Future {
        executors.taskDefs.deleteParam(executorId, id, paramName)
      }
    }
  }

  lazy val planRepo: PlanRepo = new PlanRepo() {
    def findAll: Future[Seq[Plan]] = Future {
      plans.findAll
    }

    def findForAssets(assetIds: Set[String]): Future[Seq[Plan]] = Future {
      plans.findForAssets(assetIds)
    }

    def insertOne(a: PlanAdd): Future[Either[GnError, Plan]] = Future {
      plans.insertOne(a)
    }

    def findById(id: String): Future[Either[GnError, Plan]] = Future {
      plans.findById(id)
    }

    def updateOne(id: String, u: PlanUpdate): Future[Either[GnError, String]] = Future {
      plans.updateOne(id, u)
    }

    def deleteOne(id: String): Future[Either[GnError, Plan]] = Future {
      plans.deleteOne(id)
    }

    def removeAll(): Future[String] = Future {
      plans.removeAll()
    }

    def listTasks(id: String): Future[Either[GnError, Seq[Task]]] = Future {
      plans.listTasks(id)
    }

    def getTask(id: String, taskId: String): Future[Either[GnError, Task]] = Future {
      plans.getTask(id, taskId)
    }

    def insertTask(id: String, a: TaskAdd): Future[Either[GnError, Task]] = Future {
      plans.insertTask(id, a)
    }

    def deleteTask(id: String, taskId: String): Future[Either[GnError, Task]] = Future {
      plans.deleteTask(id, taskId)
    }

    def updateTask(id: String, taskId: String, u: TaskUpdate): Future[Either[GnError, Task]] = Future {
      plans.updateTask(id, taskId, u)
    }

    def listArgs(id: String, taskId: String): Future[Either[GnError, Seq[Argument]]] = Future {
      plans.listArgs(id, taskId)
    }

    def getArg(id: String, taskId: String, paramName: String): Future[Either[GnError, Argument]] = Future {
      plans.getArg(id, taskId, paramName)
    }

    def insertArg(id: String, taskId: String, a: Argument): Future[Either[GnError, Argument]] = Future {
      plans.insertArg(id, taskId, a)
    }

    def updateArg(id: String, taskId: String, paramName: String, u: ArgumentUpdate): Future[Either[GnError, Argument]] = Future {
      plans.updateArg(id, taskId, paramName, u)
    }

    def deleteArg(id: String, taskId: String, paramName: String): Future[Either[GnError, Argument]] = Future {
      plans.deleteArg(id, taskId, paramName)
    }

  }

  def clearData(): Unit = {
    plans.removeAll()
    executors.removeAll()
    flush()
  }

  def flush(): Unit = {
    executors.flush()
    plans.flush()
  }

  def close(): Unit = flush()

  private val dataDir: File = new File(if (testing) "data/testing" else "data")
  dataDir.mkdirs()

  private val exeMap: mutable.Map[String, Executor] = mutable.Map.empty

  private def withExecutor[T](executorId: String)
                             (p: Executor ⇒ Either[GnError, T]): Either[GnError, T] = {
    exeMap.get(executorId) match {
      case Some(e) ⇒ p(e)
      case None    ⇒ Left(GnErrorF.undefined("Executor", id = executorId))
    }
  }

  private def withTaskDef[T](executorId: String, taskDefId: String)
                            (p: (Executor,TaskDef) ⇒ Either[GnError, T]): Either[GnError, T] = {
    withExecutor(executorId) { e ⇒
      e.taskDefs.find(_.taskDefId == taskDefId) match {
        case Some(td) ⇒ p(e, td)
        case None     ⇒ Left(GnErrorF.undefined("TaskDef", id = taskDefId))
      }
    }
  }

  private def withParameter[T](executorId: String, taskDefId: String, name: String)
                              (p: (Executor,TaskDef, Parameter) ⇒ Either[GnError, T]): Either[GnError, T] = {
    withTaskDef(executorId, taskDefId) { case (e, td) ⇒
      td.parameters.find(_.name == name) match {
        case Some(param) ⇒ p(e, td, param)
        case None     ⇒ Left(GnErrorF.undefined("Parameter", id = name))
      }
    }
  }


  object executors extends PxsJsonImplicits {
    private val exeFile = new File(dataDir, "executors.json")

    def load(): Unit = if (exeFile.exists()) {
      logger.debug(s"loading $exeFile")
      val contents = scala.io.Source.fromFile(exeFile).mkString
      val seq = contents.parseJson.convertTo[Seq[Executor]]
      exeMap ++= seq.map(a ⇒ a.executorId → a).toMap
    }
    private def save(): Unit = {
      logger.debug(s"saving $exeFile")
      val seq = exeMap.values.toSeq
      Files.write(exeFile.toPath,
        seq.toJson.prettyPrint.getBytes(StandardCharsets.UTF_8))
    }

    def flush(): Unit = save()

    def findAll: Either[GnError, Seq[ExecutorGetResponse]] = {
      Right(exeMap.values.toSeq.map(e ⇒
        ExecutorGetResponse(e.executorId, e.httpEndpoint, e.description))
      )
    }

    def insertOne(a: ExecutorAdd): Either[GnError, ExecutorAddResponse] = {
      exeMap.get(a.executorId) match {
        case Some(_) ⇒ Left(GnErrorF.defined("Executor", id = a.executorId))

        case None ⇒
          val e = Executor(
            executorId = a.executorId,
            httpEndpoint = a.httpEndpoint,
            description = a.description
          )
          exeMap += a.executorId → e
          Right(ExecutorAddResponse(e.executorId, e.httpEndpoint, e.description))
      }
    }

    def findById(id: String): Either[GnError, ExecutorGetResponse] = {
      withExecutor(id) { e ⇒
        Right(ExecutorGetResponse(e.executorId, e.httpEndpoint, e.description))
      }
    }

    def findByIdDetailed(id: String): Either[GnError, Executor] = {
      withExecutor(id) { e ⇒ Right(e) }
    }

    def updateOne(id: String, u: ExecutorUpdate): Either[GnError, ExecutorGetResponse] = {
      withExecutor(id) { e ⇒
        var n = e
        u.httpEndpoint foreach  { x ⇒ n = n.copy(httpEndpoint = x) }
        u.description foreach   { x ⇒ n = n.copy(description = Some(x)) }
        exeMap.update(id, n)
        Right(ExecutorGetResponse(id, n.httpEndpoint, n.description))
      }
    }

    def deleteOne(id: String): Either[GnError, ExecutorGetResponse] = {
      withExecutor(id) { e ⇒
        exeMap.remove(id)
        Right(ExecutorGetResponse(id, e.httpEndpoint, e.description))
      }
    }

    def removeAll(): Either[GnError, String] = {
      exeMap.clear()
      Right("OK")
    }

    object assets {

      def findAll(executorId: String): Either[GnError, Seq[Asset]] = {
        withExecutor(executorId) { e ⇒
          Right(e.assets)
        }
      }

      def insertOne(executorId: String, a: Asset): Either[GnError, Asset] = {
        withExecutor(executorId) { e ⇒
          e.assets.find(_.assetId == a.assetId) match {
            case Some(_) ⇒ Left(GnErrorF.defined("Asset", id = a.assetId))

            case None ⇒
              val n = e.copy(assets = e.assets :+ a)
              exeMap += executorId → n
              Right(a)
          }
        }
      }

      def findById(executorId: String, id: String): Either[GnError, Asset] = {
        withExecutor(executorId) { e ⇒
          e.assets.find(_.assetId == id) match {
            case Some(a) ⇒ Right(a)
            case None    ⇒ Left(GnErrorF.undefined("Asset", id = id))
          }
        }
      }

      def updateOne(executorId: String, id: String, u: AssetUpdate): Either[GnError, AssetUpdateResponse] = {
        withExecutor(executorId) { e ⇒
          val (found, others) = e.assets.partition(_.assetId == id)
          if (found.nonEmpty) {
            var n = found.head
            u.assetClass foreach    { x ⇒ n = n.copy(assetClass = x) }
            u.description foreach   { x ⇒ n = n.copy(description = Some(x)) }

            val newExe = e.copy(assets = others :+ n)
            exeMap.update(executorId, newExe)
            Right(AssetUpdateResponse(id))
          }
          else Left(GnErrorF.undefined("Asset", id = id))
        }
      }

      def deleteOne(executorId: String, id: String): Either[GnError, Asset] = {
        withExecutor(executorId) { e ⇒
          val (found, others) = e.assets.partition(_.assetId == id)
          if (found.nonEmpty) {
            val a = found.head
            val newExe = e.copy(assets = others)
            exeMap.update(executorId, newExe)
            Right(a)
          }
          else Left(GnErrorF.undefined("Asset", id = id))
        }
      }

      def removeAll(executorId: String): Either[GnError, String] = {
        withExecutor(executorId) { e ⇒
          val newExe = e.copy(assets = Seq.empty)
          exeMap.update(executorId, newExe)
          Right("OK")
        }
      }
    }

    object taskDefs {

      def findAll(executorId: String): Either[GnError, Seq[TaskDef]] = {
        withExecutor(executorId) { e ⇒
          Right(e.taskDefs)
        }
      }

      def insertOne(executorId: String, a: TaskDefAdd): Either[GnError, TaskDef] = {
        withExecutor(executorId) { e ⇒
          e.taskDefs.find(_.taskDefId == a.taskDefId) match {
            case Some(_) ⇒
              Left(GnErrorF.defined("TaskDef", id = a.taskDefId))

            case None ⇒
              val m = tda2td(a)
              setTaskDef(e, m)
              Right(m)
          }
        }
      }

      private def setTaskDef(e: Executor, m: TaskDef): Executor = {
        val updatedTaskDefs = m :: e.taskDefs.filterNot(_.taskDefId == m.taskDefId).toList
        setTaskDefs(e, updatedTaskDefs)
      }

      private def setTaskDefs(e: Executor, ms: Seq[TaskDef]): Executor = {
        val updatedExecutor = e.copy(taskDefs = ms)
        exeMap.update(e.executorId, updatedExecutor)
        updatedExecutor
      }

      def findById(executorId: String, id: String): Either[GnError, TaskDef] = {
        withTaskDef(executorId, id) { case (_, td) ⇒
          Right(td)
        }
      }

      def updateOne(executorId: String, id: String, u: TaskDefUpdate): Either[GnError, String] = {
        withTaskDef(executorId, id) { case (e, td) ⇒
          var n = td
          u.assetClasses  foreach  { x ⇒ n = n.copy(assetClasses  = x) }
          u.description   foreach  { x ⇒ n = n.copy(description   = Some(x)) }
          setTaskDef(e, n)
          Right("taskDef updated")
        }
      }

      def deleteOne(executorId: String, id: String): Either[GnError, TaskDef] = {
        withTaskDef(executorId, id) { case (e, td) ⇒
          setTaskDefs(e, e.taskDefs.filterNot(_.taskDefId == id))
          Right(td)
        }
      }

      def removeAll(executorId: String): Either[GnError, String] = {
        withExecutor(executorId) { e ⇒
          setTaskDefs(e, Seq.empty)
          Right("OK")
        }
      }

      def listParams(executorId: String, id: String): Either[GnError, Seq[Parameter]] = {
        withTaskDef(executorId, id) { case (_, td) ⇒
          Right(td.parameters)
        }
      }

      def getParam(executorId: String, id: String, name: String): Either[GnError, Parameter] = {
        withParameter(executorId, id, name) { case (_, _, param) ⇒
          Right(param)
        }
      }

      def addParam(executorId: String, id: String, p: Parameter): Either[GnError, Parameter] = {
        withTaskDef(executorId, id)  { case (e, td) ⇒
          td.parameters.find(_.name == p.name) match {
            case None ⇒
              val updatedTaskDef = td.copy(parameters = p :: td.parameters.toList)
              val updatedTaskDefs = updatedTaskDef :: e.taskDefs.filterNot(_.taskDefId == td.taskDefId).toList
              setTaskDefs(e, updatedTaskDefs)
              Right(p)

            case Some(_) ⇒ Left(GnErrorF.defined("Parameter", id = p.name))
          }
        }
      }

      def updateParam(executorId: String, id: String, paramName: String, u: ParameterUpdate
                     ): Either[GnError, Parameter] = {
        withParameter(executorId, id, paramName) { case (e, td, param) ⇒
          val updatedParam = paramUpdate2param(param, u)
          val updatedTaskDef = td.copy(parameters = updatedParam :: td.parameters.filterNot(_.name == paramName).toList)
          val updatedTaskDefs = updatedTaskDef :: e.taskDefs.filterNot(_.taskDefId == td.taskDefId).toList
          setTaskDefs(e, updatedTaskDefs)
          Right(param)
        }
      }

      def deleteParam(executorId: String, id: String, paramName: String
                     ): Either[GnError, Parameter] = {
        withParameter(executorId, id, paramName) { case (e, td, param) ⇒
          val updatedTaskDef = td.copy(parameters = td.parameters.filterNot(_.name == paramName).toList)
          val updatedTaskDefs = updatedTaskDef :: e.taskDefs.filterNot(_.taskDefId == td.taskDefId).toList
          setTaskDefs(e, updatedTaskDefs)
          Right(param)
        }
      }
    }
  }

  private object plans extends PxsJsonImplicits {
    def findAll: Seq[Plan] = {
      planMap.values.toSeq
    }

    def findForAssets(assetIds: Set[String]): Seq[Plan] = {
      def planHasAsset(p: Plan): Boolean = {
        p.tasks.exists(st ⇒ assetIds.contains(st.assetId))
      }
      findAll.filter(planHasAsset)
    }

    private def checkTaskAdd(t: TaskAdd): Either[GnError, Unit] = {
      withTaskDef(t.executorId, t.taskDefId) { case (e, taskDef) ⇒
        e.assets.find(_.assetId == t.assetId) match {
          case Some(asset) ⇒
            if (taskDef.assetClasses.contains(asset.assetClass))
              Right(())
            else
              Left(GnError(409,
                s"Task definition ${t.taskDefId} not applicable to asset ${t.assetId} " +
                  s"of class ${asset.assetClass}. Valid asset classes: ${taskDef.assetClasses}"
              ))

          case None ⇒ Left(GnErrorF.undefined("Asset", id = t.assetId))
        }
      }
    }

    def insertOne(a: PlanAdd): Either[GnError, Plan] = {
      val pl = a2pl(a)
      planMap += pl.planId → pl
      Right(pl)
    }

    def findById(id: String): Either[GnError, Plan] = {
      planMap.get(id) match {
        case Some(pl) ⇒ Right(pl)
        case None     ⇒ Left(GnErrorF.undefined("Plan", id = id))
      }
    }

    def updateOne(id: String, u: PlanUpdate): Either[GnError, String] = {
      planMap.get(id) match {
        case Some(pl) ⇒
          var n = pl
          u.name foreach { x ⇒ n = n.copy(name = x) }

          planMap.update(id, n)
          Right(id)

        case None     ⇒ Left(GnErrorF.undefined("Plan", id = id))
      }
    }

    def deleteOne(id: String): Either[GnError, Plan] = {
      planMap.remove(id) match {
        case Some(pl) ⇒
          Right(pl)

        case None     ⇒ Left(GnErrorF.undefined("Plan", id = id))
      }
    }

    def removeAll(): String = {
      planMap.clear()
      "OK"
    }

    def listTasks(planId: String): Either[GnError, Seq[Task]] = {
      planMap.get(planId) match {
        case Some(m) ⇒ Right(m.tasks)
        case None    ⇒ Left(GnErrorF.undefined("Plan", id = planId))
      }
    }

    def getTask(planId: String, taskId: String): Either[GnError, Task] = {
      planMap.get(planId) match {
        case Some(m) ⇒
          m.tasks.find(_.taskId == taskId) match {
            case Some(t) ⇒ Right(t)
            case None    ⇒ Left(GnErrorF.undefined("Task", id = taskId))
          }

        case None     ⇒ Left(GnErrorF.undefined("Plan", id = planId))
      }
    }

    def insertTask(planId: String, a: TaskAdd): Either[GnError, Task] = {
      planMap.get(planId) match {
        case Some(m) ⇒
          checkTaskAdd(a) match {
            case Right(_) ⇒
              val t = ta2task(a)
              val updatedPlan = m.copy(tasks = t :: m.tasks.toList)
              setModel(updatedPlan)
              Right(t)

            case Left(e) ⇒ Left(e)
          }

        case None     ⇒ Left(GnErrorF.undefined("Plan", id = planId))
      }
    }

    def deleteTask(planId: String, taskId: String): Either[GnError, Task] = {
      planMap.get(planId) match {
        case Some(m) ⇒
          m.tasks.find(_.taskId == taskId) match {
            case Some(t) ⇒
              val updatedPlan = m.copy(tasks = m.tasks.filterNot(_.taskId == taskId))
              setModel(updatedPlan)
              Right(t)

            case None ⇒ Left(GnErrorF.undefined("Task", id = taskId))
          }

        case None     ⇒ Left(GnErrorF.undefined("Plan", id = planId))
      }
    }

    def updateTask(planId: String, taskId: String, u: TaskUpdate): Either[GnError, Task] = {
      planMap.get(planId) match {
        case Some(m) ⇒
          m.tasks.find(_.taskId == taskId) match {
            case Some(t) ⇒
              // TODO  checkTaskUpdate..
              val n = tu2task(t, u)
              val updatedTasks = n :: m.tasks.filterNot(_.taskId == taskId).toList
              val updatedPlan = m.copy(tasks = updatedTasks)
              setModel(updatedPlan)
              Right(t)

            case None ⇒ Left(GnErrorF.undefined("Task", id = taskId))
          }

        case None     ⇒ Left(GnErrorF.undefined("Plan", id = planId))
      }
    }

    def listArgs(planId: String, taskId: String): Either[GnError, Seq[Argument]] = {
      planMap.get(planId) match {
        case Some(m) ⇒
          m.tasks.find(_.taskId == taskId) match {
            case Some(t) ⇒ Right(t.arguments)

            case None ⇒ Left(GnErrorF.undefined("Task", id = taskId))
          }
        case None ⇒ Left(GnErrorF.undefined("Plan", id = planId))
      }
    }

    def getArg(planId: String, taskId: String, paramName: String): Either[GnError, Argument] = {
      planMap.get(planId) match {
        case Some(m) ⇒
          m.tasks.find(_.taskId == taskId) match {
            case Some(t) ⇒
              t.arguments.find(_.paramName == paramName) match {
                case Some(a) ⇒ Right(a)

                case None ⇒ Left(GnErrorF.undefined("Argument", id = paramName))
              }
            case None ⇒ Left(GnErrorF.undefined("Task", id = taskId))
          }
        case None ⇒ Left(GnErrorF.undefined("Plan", id = planId))
      }
    }

    def insertArg(planId: String, taskId: String, a: Argument): Either[GnError, Argument] = {
      planMap.get(planId) match {
        case Some(m) ⇒
          m.tasks.find(_.taskId == taskId) match {
            case Some(t) ⇒
              t.arguments.find(_.paramName == a.paramName) match {
                case None ⇒
                  val updatedArgs = a :: t.arguments.toList
                  val updatedTask = t.copy(arguments = updatedArgs)
                  val updatedTasks = updatedTask :: m.tasks.filterNot(_.taskId == taskId).toList
                  val updatedPlan = m.copy(tasks = updatedTasks)
                  setModel(updatedPlan)
                  Right(a)

                case Some(_) ⇒ Left(GnErrorF.defined("Argument", id = a.paramName))
              }
            case None ⇒ Left(GnErrorF.undefined("Task", id = taskId))
          }

        case None ⇒ Left(GnErrorF.undefined("Plan", id = planId))
      }
    }

    def updateArg(planId: String, taskId: String, paramName: String, u: ArgumentUpdate): Either[GnError, Argument] = {
      planMap.get(planId) match {
        case Some(m) ⇒
          m.tasks.find(_.taskId == taskId) match {
            case Some(t) ⇒
              t.arguments.find(_.paramName == paramName) match {
                case Some(a) ⇒
                  val updatedArg = argu2arg(a, u)
                  val updatedArgs = updatedArg :: t.arguments.filterNot(_.paramName == paramName).toList
                  val updatedTask = t.copy(arguments = updatedArgs)
                  val updatedTasks = updatedTask :: m.tasks.filterNot(_.taskId == taskId).toList
                  val updatedPlan = m.copy(tasks = updatedTasks)
                  setModel(updatedPlan)
                  Right(a)

                case None ⇒ Left(GnErrorF.undefined("Argument", id = paramName))
              }
            case None ⇒ Left(GnErrorF.undefined("Task", id = taskId))
          }
        case None ⇒ Left(GnErrorF.undefined("Plan", id = planId))
      }
    }

    def deleteArg(planId: String, taskId: String, paramName: String): Either[GnError, Argument] = {
      planMap.get(planId) match {
        case Some(m) ⇒
          m.tasks.find(_.taskId == taskId) match {
            case Some(t) ⇒
              t.arguments.find(_.paramName == paramName) match {
                case Some(a)    ⇒
                  val updatedArgs = t.arguments.filterNot(_.paramName == paramName)
                  val updatedTask = t.copy(arguments = updatedArgs)
                  val updatedTasks = updatedTask :: m.tasks.filterNot(_.taskId == taskId).toList
                  val updatedPlan = m.copy(tasks = updatedTasks)
                  setModel(updatedPlan)
                  Right(a)

                case None ⇒ Left(GnErrorF.undefined("Argument", id = paramName))
              }
            case None ⇒ Left(GnErrorF.undefined("Task", id = taskId))
          }
        case None ⇒ Left(GnErrorF.undefined("Plan", id = planId))
      }
    }

    private def setModel(plan: Plan): Unit = {
      planMap.update(plan.planId, plan)
    }

    def flush(): Unit = doSave()

    def close(): Unit = flush()

    private def a2pl(a: PlanAdd): Plan =
      Plan(
        randomId,
        name        = a.name,
        description = a.description,
      )

    private val planMap: mutable.Map[String, Plan] = mutable.Map.empty

    private val file = new File(dataDir, "plans.json")

    def load(): Unit = if (file.exists()) {
      logger.debug(s"loading $file")
      val contents = scala.io.Source.fromFile(file).mkString
      val seq = contents.parseJson.convertTo[Seq[Plan]]
      planMap ++= seq.map(x ⇒ x.planId → x).toMap
    }

    private def doSave(): Unit = {
      logger.debug(s"saving $file")
      val seq = planMap.values.toSeq
      Files.write(file.toPath,
        seq.toJson.prettyPrint.getBytes(StandardCharsets.UTF_8))
    }
  }

  executors.load()
  plans.load()
}
