package pxs.data.impl.mongo

import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.bson.conversions.Bson
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.model.Updates.{combine, set}
import org.mongodb.scala.{Completed, Document, MongoClient, MongoCollection, MongoDatabase, Observer}
import pxs.config.MongoCfg
import pxs.data.PlanRepo
import pxs.model._
import spray.json._

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.control.NonFatal

case class MgPlan(
                   _id:            ObjectId,
                   title:          String,
                   description:    Option[String] = None,
                   tasks:          Seq[Task] = Seq.empty
                 )

class PlanRepoMongo(mCfg: MongoCfg)
                   (implicit val ec: ExecutionContext)
  extends PlanRepo with PxsJsonImplicits {

  def findAll: Future[Seq[Plan]] = {
    collection.find().map(convert).toFuture()
  }

  def findForAssets(assetIds: Set[String]): Future[Seq[Plan]] = {
    // TODO use some mongo filter
    def planHasAsset(p: Plan): Boolean =
      p.tasks.exists(st ⇒ assetIds.contains(st.assetId))

    findAll.map(_.filter(planHasAsset))
  }

  def find(filter: Bson): Future[Seq[Plan]] = {
    collection.find(filter).map(convert).toFuture()
  }

  def insertOne(a: PlanAdd): Future[Either[GnError, Plan]] = {
    val p = Promise[Either[GnError, Plan]]()

    val g = convert(a)
    collection.insertOne(g).subscribe(new Observer[Completed] {
      override def onNext(result: Completed): Unit = {
        //println(s"MongoDb: insertOne: saved.")
        p.success(Right(convert(g)))
      }

      override def onError(e: Throwable): Unit = {
        p.success(Left(GnError(500, s"error inserting $modelName: ${e.getMessage}")))
      }

      override def onComplete(): Unit = {
        //println(s"MongoDb: insertOne: onComplete.")
      }
    })

    p.future
  }

  def findById(id: String): Future[Either[GnError, Plan]] = {
    val p = Promise[Either[GnError, Plan]]()
    parseId(id) match {
      case Right(oid) ⇒ doFindById(oid, p)
      case Left(e)    ⇒ p.success(Left(e))
    }
    p.future
  }

  def updateOne(id: String, u: PlanUpdate): Future[Either[GnError, String]] = {
    val p = Promise[Either[GnError, String]]()

    findById(id) map {
      case Right(_) ⇒
        val doc = toDocument(u)
        val updates = doc.toList.map {
          case (field, data) ⇒ set(field, data)
        }

        collection.updateOne(
          equal("_id", new ObjectId(id)),
          combine(updates: _*)
        ).toFuture().map { x ⇒
          if (x.getModifiedCount == 1)
            p.success(Right(id))
          else
            p.success(Left(GnError(500, s"could not update $modelName")))
        }

      case Left(e) ⇒
        p.success(Left(e))
    }
    p.future
  }

  def deleteOne(id: String): Future[Either[GnError, Plan]] = {
    val p = Promise[Either[GnError, Plan]]()
    findById(id) map {
      case Right(t) ⇒
        collection.deleteOne(equal("_id", new ObjectId(id))).toFuture().map { _ ⇒
          p.success(Right(t))
        }

      case Left(e) ⇒
        p.success(Left(e))
    }
    p.future
  }

  def removeAll(): Future[String] = {
    collection.drop().toFuture().map(_ ⇒ "OK")
  }

  def listTasks(id: String): Future[Either[GnError, Seq[Task]]] = ???
  def getTask(id: String, taskId: String): Future[Either[GnError, Task]] = ???
  def insertTask(id: String, a: TaskAdd): Future[Either[GnError, Task]] = ???
  def deleteTask(planId: String, taskId: String): Future[Either[GnError, Task]] = ???
  def updateTask(id: String, taskId: String, u: TaskUpdate): Future[Either[GnError, Task]] = ???

  def listArgs(id: String, taskId: String): Future[Either[GnError, Seq[Argument]]] = ???
  def getArg(id: String, taskId: String, paramName: String): Future[Either[GnError, Argument]] = ???
  def insertArg(id: String, taskId: String, a: Argument): Future[Either[GnError, Argument]] = ???
  def deleteArg(planId: String, taskId: String, paramName: String): Future[Either[GnError, Argument]] = ???
  def updateArg(id: String, taskId: String, paramName: String, u: ArgumentUpdate): Future[Either[GnError, Argument]] = ???

  def flush(): Unit = ()

  def close(): Unit = mongoClient.close()

  private def toJson(u: PlanUpdate): JsValue = u.toJson

  private def convert(g: MgPlan): Plan =
    Plan(
      g._id.toHexString,
      name       = g.title,
      description = g.description,
      tasks       = g.tasks
    )

  private def convert(a: PlanAdd): MgPlan =
    MgPlan(
      new ObjectId(),
      title       = a.name,
      description = a.description,
    )

  private def toDocument(u: PlanUpdate): Document =
    Document(JsObject(toJson(u).asJsObject.fields.filterKeys(_ != "_id")).compactPrint)

  private def doFindById(id: ObjectId, p: Promise[Either[GnError, Plan]]): Unit = {
    collection.find(equal("_id", id)).first().subscribe(new Observer[MgPlan] {
      def onNext(g: MgPlan): Unit =
        p.success(Right(convert(g)))

      def onComplete(): Unit =
        p.success(Left(GnErrorF.undefined(modelName, id.toHexString)))

      def onError(e: Throwable): Unit =
        p.success(Left(GnError(500, s"retrieving $modelName: ${e.getMessage}",
          id = Some(id.toHexString))))
    })
  }

  private def parseId(id: String): Either[GnError, ObjectId] = {
    try Right(new ObjectId(id))
    catch {
      case NonFatal(e) ⇒ Left(GnError(400,
        s"Invalid $modelName ID: ${e.getMessage}", id = Some(id)))
    }
  }
  private val mongoClient: MongoClient = {
    /** see [[com.mongodb.ConnectionString]] */
    val uri = "mongodb://" + (mCfg.username map { u ⇒
      val usrPw = u + mCfg.password.map(":" + _).getOrElse("") + "@"
      usrPw + mCfg.host + "/" + mCfg.database
    }).getOrElse(mCfg.host)

    MongoClient(uri)
  }

  private val database: MongoDatabase = {
    import org.bson.codecs.configuration.CodecRegistries.fromRegistries
    import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY

    val codecRegistry = fromRegistries(
      PlanRepoMongo.codecRegistry,
      DEFAULT_CODEC_REGISTRY
    )
    mongoClient.getDatabase(mCfg.database).withCodecRegistry(codecRegistry)
  }

  private val modelName = "Plan"
  private val collection: MongoCollection[MgPlan] = database.getCollection(mCfg.plans)

}

object PlanRepoMongo {
  def codecRegistry: CodecRegistry = {
    import org.bson.codecs.configuration.CodecRegistries.fromProviders
    import org.mongodb.scala.bson.codecs.Macros._

    fromProviders(
      createCodecProviderIgnoreNone[Task](),
      createCodecProviderIgnoreNone[MgPlan]()
    )
  }
}
