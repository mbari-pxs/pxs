package pxs.data

import com.typesafe.scalalogging.{LazyLogging ⇒ Logging}
import pxs.data.impl.DbMem

import scala.concurrent.ExecutionContext

class DbFactory(implicit ec: ExecutionContext) extends Logging {

  def openDb: DbInterface = {
    // TODO
    new DbMem()
  }

  def testDb: DbInterface = {
    // TODO
    new DbMem(testing = true)

    //cfg.mongo match {
    //  case None ⇒ new DbMem()
    //  case Some(mCfg) ⇒
    //    new PlanRepoMongo(
    //      mCfg.copy(
    //        host = sys.env.getOrElse("MONGO_HOST", "localhost"),
    //        database = "pxs_test"
    //      )
    //    )
    //}
  }
}
