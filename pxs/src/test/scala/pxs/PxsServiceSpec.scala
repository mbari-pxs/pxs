package pxs

import akka.http.scaladsl.model.ContentTypes._
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{Matchers, WordSpec}
import pxs.data._
import pxs.model._
import pxs.server._

class PxsServiceSpec extends WordSpec with Matchers with ScalatestRouteTest with Service {
  val dbFactory: DbFactory = new pxs.data.DbFactory()
  val db: DbInterface = dbFactory.testDb
  val executorRepo: ExecutorRepo = db.executorRepo
  val planRepo:     PlanRepo    = db.planRepo

  def routes: Route = allRoutes

  override def beforeAll(): Unit = {
    super.beforeAll()
    db.clearData()
    println(fansi.Color.Yellow(s"Database: ${db.details}"))
  }

  override def afterAll(): Unit = {
    db.close()
    super.afterAll()
  }

  val executors: Seq[ExecutorAdd] = (1 to 2).map { n ⇒
    val executorId = s"executor_$n"
    val httpEndpoint = s"http://$executorId"
    ExecutorAdd(
      executorId,
      httpEndpoint,
      Some(s"Description of executor $executorId")
    )
  }

  // some common assets
  val assets: Seq[Asset] = {
    (1 to 2).map(n ⇒ Asset(
      assetId = s"asset_$n",
      assetClass = "LRAUV",
      description = Some(s"asset_$n description")
    ))
  }

  val parameters = Seq(
    Parameter(
      name   = "param1",
      `type` = "double",
      required = true
    ),
    Parameter(
      name   = "param2",
      `type` = "boolean",
      required = true,
      defaultValue = Some("false")
    ),
    Parameter(
      name   = "param3",
      `type` = "integer",
      required = false
    )
  )

  // lets define one TaskDef per Asset above
  val taskDefs: Seq[TaskDefAdd] = assets.map { a ⇒
    val taskDefId = s"taskDef_${a.assetId}"
    TaskDefAdd(
      taskDefId = taskDefId,
      assetClasses = Seq(a.assetClass),
      description = Some(s"$taskDefId description")
    )
  }

  val plan1Tasks = Seq(
    TaskAdd(
      executorId = executors.head.executorId,
      taskDefId = taskDefs.head.taskDefId,
      assetId = assets.head.assetId
    )
  )

  val plan1 = PlanAdd(
    name  = "a plan",
    description = Some("descr")
  )

  "Combined service" should {

    "add some executors" in {
      executors foreach { a ⇒
        Post(s"/api/executors", a) ~> routes ~> check {
          status shouldBe Created
          contentType shouldBe `application/json`
          val r = responseAs[ExecutorAddResponse]
          r.executorId shouldBe a.executorId
        }
      }
    }

    "get defined executors" in {
      executors foreach { a ⇒
        Get(s"/api/executors/${a.executorId}") ~> routes ~> check {
          status shouldBe OK
          contentType shouldBe `application/json`
          val r = responseAs[ExecutorGetResponse]
          r.executorId shouldBe a.executorId
          r.httpEndpoint shouldBe a.httpEndpoint
          r.description shouldBe a.description
        }
      }
    }

    "list executors" in {
      Get(s"/api/executors") ~> routes ~> check {
        status shouldBe OK
        contentType shouldBe `application/json`
        val seq = responseAs[Seq[ExecutorGetResponse]]
        seq.length     shouldBe executors.length
      }
    }

    "add some assets" in {
      // let's add same set of assets for each executor above
      executors foreach { x ⇒
        assets foreach { a ⇒
          Post(s"/api/executors/${x.executorId}/assets", a) ~> routes ~> check {
            status shouldBe Created
            contentType shouldBe `application/json`
            val r = responseAs[Asset]
            r.description shouldBe a.description
          }
        }
      }
    }

    "get defined assets" in {
      executors foreach { x ⇒
        assets foreach { a ⇒
          Get(s"/api/executors/${x.executorId}/assets/${a.assetId}") ~> routes ~> check {
            //println(fansi.Color.LightCyan("XXXX " +responseEntity))
            status shouldBe OK
            contentType shouldBe `application/json`
            val r = responseAs[Asset]
            r.assetId shouldBe a.assetId
            r.assetClass shouldBe a.assetClass
            r.description shouldBe a.description
          }
        }
      }
    }

    "add some taskDefs" in {
      executors foreach { x ⇒
        taskDefs foreach { td ⇒
          Post(s"/api/executors/${x.executorId}/taskdefs", td) ~> routes ~> check {
            status shouldBe Created
            contentType shouldBe `application/json`
            val r = responseAs[TaskDefAdd]
            r.taskDefId shouldBe td.taskDefId
          }
        }
      }
    }

    "get defined taskDefs" in {
      executors foreach { x ⇒
        taskDefs foreach { td ⇒
          Get(s"/api/executors/${x.executorId}/taskdefs/${td.taskDefId}") ~> routes ~> check {
            status shouldBe OK
            contentType shouldBe `application/json`
            val r = responseAs[TaskDef]
            r.taskDefId shouldBe td.taskDefId
          }
        }
      }
    }

    val params: Seq[(String, String, Parameter)] = for {
      x ← executors
      td ← taskDefs
      p ← parameters
    } yield (x.executorId, td.taskDefId, p)

    "add parameters" in {
      params foreach { case (executorId, taskDefId, p) ⇒
        Post(s"/api/executors/$executorId/taskdefs/$taskDefId/parameters", p) ~> routes ~> check {
          status shouldBe Created
          contentType shouldBe `application/json`
          val r = responseAs[ParameterAddResponse]
          r.name shouldBe p.name
        }
      }
    }

    "get parameters" in {
      params foreach { case (executorId, taskDefId, p) ⇒
        Get(s"/api/executors/$executorId/taskdefs/$taskDefId/parameters/${p.name}") ~> routes ~> check {
          status shouldBe OK
          contentType shouldBe `application/json`
          val r = responseAs[Parameter]
          r shouldBe p
        }
      }
    }

    "update parameters" in {
      params foreach { case (executorId, taskDefId, p) ⇒
        val u = ParameterUpdate(
          defaultValue = Some(s"dummyValue_${p.name}")
        )
        Put(s"/api/executors/$executorId/taskdefs/$taskDefId/parameters/${p.name}", u) ~> routes ~> check {
          status shouldBe OK
          contentType shouldBe `application/json`
          val r = responseAs[ParameterUpdateResponse]
          r.name shouldBe p.name
        }
      }
    }

    "delete parameter" in {
      // let's remove last param added above
      params.reverse.headOption foreach { case (executorId, taskDefId, p) ⇒
        Delete(s"/api/executors/$executorId/taskdefs/$taskDefId/parameters/${p.name}") ~> routes ~> check {
          status shouldBe OK
          contentType shouldBe `application/json`
          val r = responseAs[ParameterDeleteResponse]
          r.name shouldBe p.name
        }
      }
    }

    var addPlan1Res: Option[PlanAddResponse] = None

    "add plan" in {
      Post(s"/api/plans", plan1) ~> routes ~> check {
        status shouldBe Created
        contentType shouldBe `application/json`
        val r = responseAs[PlanAddResponse]
        addPlan1Res = Some(r)
        r.name === plan1.name
      }
    }

    "get defined plan" in {
      assert(addPlan1Res.isDefined)
      addPlan1Res map { planAddResponse ⇒
        val planId = planAddResponse.planId
        Get(s"/api/plans/$planId") ~> routes ~> check {
          status shouldBe OK
          contentType shouldBe `application/json`
          val r = responseAs[Plan]
          r.planId === planAddResponse.planId
          r.name === "a valid plan"
        }
      }
    }

    "get lists of tasks of defined plan" in {
      assert(addPlan1Res.isDefined)
      addPlan1Res map { planAddResponse ⇒
        val planId = planAddResponse.planId
        Get(s"/api/plans/$planId/tasks") ~> routes ~> check {
          status shouldBe OK
          contentType shouldBe `application/json`
          val r = responseAs[Seq[Task]]
          r.length === plan1Tasks.length
        }
      }
    }

    var addTaskRes: Option[TaskAddResponse] = None

    "add task to an existing plan" in {
      assert(addPlan1Res.isDefined)
      addPlan1Res map { planAddResponse ⇒
        val planId = planAddResponse.planId

        // use first task in plan1 as a basis for new task:
        val base = plan1Tasks.head
        val newTask = TaskAdd(
          taskDefId = base.taskDefId,
          executorId = base.executorId,
          assetId = base.assetId
        )

        Post(s"/api/plans/$planId/tasks", newTask) ~> routes ~> check {
          status shouldBe Created
          contentType shouldBe `application/json`
          val r = responseAs[TaskAddResponse]
          addTaskRes = Some(r)
          r.planId === planId
        }
      }
    }

    var addedArgs: List[Argument] = List.empty

    "add valid arguments" in {
      assert(addTaskRes.isDefined)
      addTaskRes foreach { taskAddResponse ⇒
        val planId = taskAddResponse.planId
        val taskId = taskAddResponse.taskId

        parameters.take(2) foreach { p ⇒
          val arg = Argument(
            paramName = p.name,
            paramValue = "someValue" // TODO compatible with p.`type`
          )

          Post(s"/api/plans/$planId/tasks/$taskId/arguments", arg) ~> routes ~> check {
            status shouldBe Created
            contentType shouldBe `application/json`
            val r = responseAs[Argument]
            addedArgs = addedArgs ++ List(r)
            r === arg
          }
        }
      }
    }

    "get added arguments" in {
      assert(addedArgs.nonEmpty)
      addedArgs.length === parameters.take(2).length

      addTaskRes foreach { taskAddResponse ⇒
        val planId = taskAddResponse.planId
        val taskId = taskAddResponse.taskId

        addedArgs.zip(parameters.take(2)) foreach { case (a, p) ⇒
          a.paramName === p.name

          Get(s"/api/plans/$planId/tasks/$taskId/arguments/${a.paramName}") ~> routes ~> check {
            status shouldBe OK
            contentType shouldBe `application/json`
            val r = responseAs[Argument]
            addedArgs = r :: addedArgs
            r === a
          }
        }
      }
    }

    "fail to get non-existing argument" in {
      assert(addedArgs.nonEmpty)
      addedArgs.length === parameters.length

      addTaskRes foreach { taskAddResponse ⇒
        val planId = taskAddResponse.planId
        val taskId = taskAddResponse.taskId

        Get(s"/api/plans/$planId/tasks/$taskId/arguments/non-existing-arg") ~> routes ~> check {
          status shouldBe NotFound
        }
      }
    }

    "update arguments" in {
      assert(addedArgs.nonEmpty)
      addedArgs.length === parameters.take(2).length

      addTaskRes foreach { taskAddResponse ⇒
        val planId = taskAddResponse.planId
        val taskId = taskAddResponse.taskId

        addedArgs foreach { a ⇒
          val u = ArgumentUpdate(
            paramValue = "modified-arg-value"
          )

          Put(s"/api/plans/$planId/tasks/$taskId/arguments/${a.paramName}", u) ~> routes ~> check {
            status shouldBe OK
            contentType shouldBe `application/json`
            val r = responseAs[Argument]
            r.paramValue === u.paramValue
          }
        }
      }
    }

    "delete arguments" in {
      assert(addedArgs.nonEmpty)
      addedArgs.length === parameters.length

      addTaskRes foreach { taskAddResponse ⇒
        val planId = taskAddResponse.planId
        val taskId = taskAddResponse.taskId

        // let's remove the first arg
        val a = addedArgs.head
        Delete(s"/api/plans/$planId/tasks/$taskId/arguments/${a.paramName}") ~> routes ~> check {
          status shouldBe OK
          contentType shouldBe `application/json`
          val r = responseAs[Argument]
          r === a
        }
      }
    }

    "get individual tasks of defined plan" in {
      assert(addTaskRes.isDefined)
      assert(addPlan1Res.isDefined)
      for {
        planAddResponse ← addPlan1Res
        taResponse ← addTaskRes
      } {
        val planId = planAddResponse.planId
        val taskId = taResponse.taskId
        Get(s"/api/plans/$planId/tasks/$taskId") ~> routes ~> check {
          status shouldBe OK
          contentType shouldBe `application/json`
          val r = responseAs[Task]
          r.taskId === taskId
        }
      }
    }

    "get singleton plan list for just added task by given asset ID" in {
      Get(s"/api/plans?assets=${assets.head.assetId}") ~> routes ~> check {
        status shouldBe OK
        contentType shouldBe `application/json`
        val seq = responseAs[Seq[Plan]]
        seq.length shouldBe 1
      }
    }

    "get task in existing plan" in {
      addTaskRes map { taResponse ⇒
        val planId = taResponse.planId
        val taskId = taResponse.taskId
        Get(s"/api/plans/$planId/tasks/$taskId") ~> routes ~> check {
          status shouldBe OK
          contentType shouldBe `application/json`
          val r = responseAs[Task]
          r.taskId === taskId
        }
      }
    }

    var updateTaskRes: Option[TaskUpdateResponse] = None

    "update task in existing plan" in {
      addTaskRes map { taResponse ⇒
        val planId = taResponse.planId
        val taskId = taResponse.taskId
        val u = TaskUpdate(

        )
        Put(s"/api/plans/$planId/tasks/$taskId", u) ~> routes ~> check {
          status shouldBe OK
          contentType shouldBe `application/json`
          val r = responseAs[TaskUpdateResponse]
          updateTaskRes = Some(r)
          r.taskId === taskId
        }
      }
    }

    "delete task in existing plan" in {
      updateTaskRes map { tuResponse ⇒
        val planId = tuResponse.planId
        val taskId = tuResponse.taskId
        Delete(s"/api/plans/$planId/tasks/$taskId") ~> routes ~> check {
          status shouldBe OK
          contentType shouldBe `application/json`
          val r = responseAs[TaskDeleteResponse]
          r.planId === planId
          r.taskId === taskId
        }
      }
    }

    "get empty plan list for non-existing assetId" in {
      Get(s"/api/plans?assets=non-existing-asset") ~> routes ~> check {
        status shouldBe OK
        contentType shouldBe `application/json`
        val seq = responseAs[Seq[Plan]]
        seq.length shouldBe 0
      }
    }

    "fail to add plan with non-existing executor" in {
      assert(addPlan1Res.isDefined)
      addPlan1Res map { planAddResponse ⇒
        val planId = planAddResponse.planId

        // use first task in plan1 as a basis for new task:
        val base = plan1Tasks.head
        val newTask = base.copy(
          executorId = "non-existing-executor"
        )

        Post(s"/api/plans/$planId/tasks", newTask) ~> routes ~> check {
          status shouldBe NotFound
          contentType shouldBe `application/json`
          responseAs[GnError]
        }
      }
    }

    "fail to add plan with non-existing taskDef" in {
      assert(addPlan1Res.isDefined)
      addPlan1Res map { planAddResponse ⇒
        val planId = planAddResponse.planId

        // use first task in plan1 as a basis for new task:
        val base = plan1Tasks.head
        val newTask = base.copy(
          taskDefId = "non-existing-taskDef"
        )

        Post(s"/api/plans/$planId/tasks", newTask) ~> routes ~> check {
          status shouldBe NotFound
          contentType shouldBe `application/json`
          responseAs[GnError]
        }
      }
    }

    "fail to add plan with non-existing asset" in {
      assert(addPlan1Res.isDefined)
      addPlan1Res map { planAddResponse ⇒
        val planId = planAddResponse.planId

        // use first task in plan1 as a basis for new task:
        val base = plan1Tasks.head
        val newTask = base.copy(
          assetId = "non-existing-assetId"
        )

        Post(s"/api/plans/$planId/tasks", newTask) ~> routes ~> check {
          status shouldBe NotFound
          contentType shouldBe `application/json`
          responseAs[GnError]
        }
      }
    }
  }
}
