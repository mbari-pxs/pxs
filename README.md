[![Build Status](https://travis-ci.org/mbari-pxs/pxs.svg?branch=master)](https://travis-ci.org/mbari-pxs/pxs)

# PXS

Preliminaries towards a Planning Execution Service ...

Main initial goals:

- Provide CRUD (Create-Read-Update-Delete) operations for execution
  providers regarding mission task definitions.
- Support mediation services such that client applications can
  schedule plans and tasks and request their execution through PXS.
